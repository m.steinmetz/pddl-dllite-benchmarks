(define (domain grounded-SIMPLE-ADL-ROBOT)
(:requirements
:strips
)
(:predicates
(Flag42-)
(Flag38-)
(Flag36-)
(Flag25-)
(Flag23-)
(Flag22-)
(Flag21-)
(Flag8-)
(Flag4-)
(Flag2-)
(ROW3-ROBOT)
(ROW2-ROBOT)
(ROW1-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(ROW0-ROBOT)
(BELOWOF1-ROBOT)
(Flag40-)
(Flag39-)
(Flag37-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag3-)
(ERROR-)
(COLUMN3-ROBOT)
(COLUMN2-ROBOT)
(COLUMN1-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(COLUMN0-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(LEFTOF1-ROBOT)
(Flag44-)
(Flag43-)
(Flag41-)
(Flag35-)
(Flag33-)
(Flag32-)
(Flag31-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag24-)
(Flag20-)
(Flag16-)
(Flag1-)
(Flag45-)
(Flag34-)
(Flag30-)
(NOT-COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF0-ROBOT)
(NOT-LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(NOT-CHECKCONSISTENCY-)
(NOT-COLUMN3-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(NOT-ERROR-)
(NOT-ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(NOT-BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(NOT-ROW3-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(Flag42prime-)
(Flag38prime-)
(Flag36prime-)
(Flag25prime-)
(Flag23prime-)
(Flag22prime-)
(Flag21prime-)
(Flag8prime-)
(Flag4prime-)
(Flag2prime-)
(Flag40prime-)
(Flag39prime-)
(Flag37prime-)
(Flag19prime-)
(Flag18prime-)
(Flag17prime-)
(Flag15prime-)
(Flag14prime-)
(Flag13prime-)
(Flag12prime-)
(Flag11prime-)
(Flag10prime-)
(Flag9prime-)
(Flag7prime-)
(Flag6prime-)
(Flag5prime-)
(Flag3prime-)
(Flag44prime-)
(Flag43prime-)
(Flag41prime-)
(Flag35prime-)
(Flag33prime-)
(Flag32prime-)
(Flag31prime-)
(Flag29prime-)
(Flag28prime-)
(Flag27prime-)
(Flag26prime-)
(Flag24prime-)
(Flag20prime-)
(Flag16prime-)
(Flag1prime-)
(Flag45prime-)
(Flag34prime-)
(Flag30prime-)
)
(:action Flag30Action-0
:parameters ()
:precondition
(and
(Flag26-)
(Flag26prime-)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-1
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-2
:parameters ()
:precondition
(and
(Flag28-)
(Flag28prime-)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-3
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Prim30Action
:parameters ()
:precondition
(and
(not (Flag26-))
(Flag26prime-)
(not (Flag27-))
(Flag27prime-)
(not (Flag28-))
(Flag28prime-)
(not (Flag29-))
(Flag29prime-)
)
:effect
(and
(Flag30prime-)
(not (Flag30-))
)

)
(:action Flag34Action-0
:parameters ()
:precondition
(and
(Flag31-)
(Flag31prime-)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Flag34Action-1
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Flag34Action-2
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Flag34Action-3
:parameters ()
:precondition
(and
(Flag32-)
(Flag32prime-)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Flag34Action-4
:parameters ()
:precondition
(and
(Flag28-)
(Flag28prime-)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Flag34Action-5
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Prim34Action
:parameters ()
:precondition
(and
(not (Flag31-))
(Flag31prime-)
(not (Flag27-))
(Flag27prime-)
(not (Flag29-))
(Flag29prime-)
(not (Flag32-))
(Flag32prime-)
(not (Flag28-))
(Flag28prime-)
(not (Flag33-))
(Flag33prime-)
)
:effect
(and
(Flag34prime-)
(not (Flag34-))
)

)
(:action Flag37Action-0
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(Flag37-)
(Flag37prime-)
)

)
(:action Flag37Action-1
:parameters ()
:precondition
(and
(Flag32-)
(Flag32prime-)
)
:effect
(and
(Flag37-)
(Flag37prime-)
)

)
(:action Flag37Action-2
:parameters ()
:precondition
(and
(Flag28-)
(Flag28prime-)
)
:effect
(and
(Flag37-)
(Flag37prime-)
)

)
(:action Flag37Action-3
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag37-)
(Flag37prime-)
)

)
(:action Flag37Action-4
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag37-)
(Flag37prime-)
)

)
(:action Prim37Action
:parameters ()
:precondition
(and
(not (Flag29-))
(Flag29prime-)
(not (Flag32-))
(Flag32prime-)
(not (Flag28-))
(Flag28prime-)
(not (Flag33-))
(Flag33prime-)
(not (Flag35-))
(Flag35prime-)
(not (Flag36-))
(Flag36prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Flag45Action-0
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag45Action-1
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag45Action-2
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag45Action-3
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Prim45Action
:parameters ()
:precondition
(and
(not (Flag43-))
(Flag43prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag33-))
(Flag33prime-)
(not (Flag29-))
(Flag29prime-)
)
:effect
(and
(Flag45prime-)
(not (Flag45-))
)

)
(:action Flag1Action
:parameters ()
:precondition
(and
(COLUMN2-ROBOT)
(ROW1-ROBOT)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Prim1Action-0
:parameters ()
:precondition
(and
(not (COLUMN2-ROBOT))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Flag2Action-0
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-2
:parameters ()
:precondition
(and
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-3
:parameters ()
:precondition
(and
(RIGHTOF0-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-4
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-6
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-7
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Prim2Action
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
(not (LEFTOF3-ROBOT))
(not (RIGHTOF1-ROBOT))
(not (RIGHTOF0-ROBOT))
(not (LEFTOF2-ROBOT))
(not (RIGHTOF2-ROBOT))
(not (LEFTOF1-ROBOT))
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag2prime-)
(not (Flag2-))
)

)
(:action Flag12Action-0
:parameters ()
:precondition
(and
(Flag9-)
(Flag9prime-)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-2
:parameters ()
:precondition
(and
(Flag10-)
(Flag10prime-)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-3
:parameters ()
:precondition
(and
(Flag11-)
(Flag11prime-)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Prim12Action
:parameters ()
:precondition
(and
(not (Flag9-))
(Flag9prime-)
(not (BELOWOF1-ROBOT))
(not (Flag10-))
(Flag10prime-)
(not (Flag11-))
(Flag11prime-)
)
:effect
(and
(Flag12prime-)
(not (Flag12-))
)

)
(:action Flag16Action-0
:parameters ()
:precondition
(and
(Flag9-)
(Flag9prime-)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-1
:parameters ()
:precondition
(and
(Flag13-)
(Flag13prime-)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-2
:parameters ()
:precondition
(and
(Flag10-)
(Flag10prime-)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-3
:parameters ()
:precondition
(and
(Flag11-)
(Flag11prime-)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-4
:parameters ()
:precondition
(and
(Flag14-)
(Flag14prime-)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-5
:parameters ()
:precondition
(and
(Flag15-)
(Flag15prime-)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Prim16Action
:parameters ()
:precondition
(and
(not (Flag9-))
(Flag9prime-)
(not (Flag13-))
(Flag13prime-)
(not (Flag10-))
(Flag10prime-)
(not (Flag11-))
(Flag11prime-)
(not (Flag14-))
(Flag14prime-)
(not (Flag15-))
(Flag15prime-)
)
:effect
(and
(Flag16prime-)
(not (Flag16-))
)

)
(:action Flag20Action-0
:parameters ()
:precondition
(and
(Flag9-)
(Flag9prime-)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-1
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-2
:parameters ()
:precondition
(and
(Flag18-)
(Flag18prime-)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-3
:parameters ()
:precondition
(and
(Flag19-)
(Flag19prime-)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-4
:parameters ()
:precondition
(and
(Flag11-)
(Flag11prime-)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-5
:parameters ()
:precondition
(and
(Flag14-)
(Flag14prime-)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Prim20Action
:parameters ()
:precondition
(and
(not (Flag9-))
(Flag9prime-)
(not (Flag17-))
(Flag17prime-)
(not (Flag18-))
(Flag18prime-)
(not (Flag19-))
(Flag19prime-)
(not (Flag11-))
(Flag11prime-)
(not (Flag14-))
(Flag14prime-)
)
:effect
(and
(Flag20prime-)
(not (Flag20-))
)

)
(:action Flag21Action-0
:parameters ()
:precondition
(and
(RIGHTOF0-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-1
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-2
:parameters ()
:precondition
(and
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Prim21Action
:parameters ()
:precondition
(and
(not (RIGHTOF0-ROBOT))
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF1-ROBOT))
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Flag22Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-1
:parameters ()
:precondition
(and
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Prim22Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF1-ROBOT))
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag22prime-)
(not (Flag22-))
)

)
(:action Flag23Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Prim23Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag23prime-)
(not (Flag23-))
)

)
(:action Flag24Action-0
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-1
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Prim24Action
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag24prime-)
(not (Flag24-))
)

)
(:action Flag25Action-0
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-2
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Prim25Action
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Flag26Action
:parameters ()
:precondition
(and
(RIGHTOF0-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag27Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag28Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag29Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Prim26Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF0-ROBOT))
)
:effect
(and
(Flag26prime-)
(not (Flag26-))
)

)
(:action Prim26Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag26prime-)
(not (Flag26-))
)

)
(:action Prim27Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim27Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF1-ROBOT))
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim28Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag28prime-)
(not (Flag28-))
)

)
(:action Prim29Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim29Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Flag31Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag32Action
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag33Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Prim31Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim31Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF1-ROBOT))
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim32Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag32prime-)
(not (Flag32-))
)

)
(:action Prim33Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag33prime-)
(not (Flag33-))
)

)
(:action Prim33Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag33prime-)
(not (Flag33-))
)

)
(:action Flag35Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Prim35Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Flag41Action-0
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag41Action-1
:parameters ()
:precondition
(and
(Flag14-)
(Flag14prime-)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag41Action-2
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag41Action-3
:parameters ()
:precondition
(and
(Flag11-)
(Flag11prime-)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Prim41Action
:parameters ()
:precondition
(and
(not (Flag39-))
(Flag39prime-)
(not (Flag14-))
(Flag14prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag11-))
(Flag11prime-)
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Flag42Action-0
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Flag42Action-2
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Flag42Action-3
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Prim42Action
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag42prime-)
(not (Flag42-))
)

)
(:action Flag43Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Flag44Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag44-)
(Flag44prime-)
)

)
(:action Prim43Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-1
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim44Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action MOVELEFT-ROBOT
:parameters ()
:precondition
(and
(Flag3-)
(Flag3prime-)
(Flag45prime-)
(Flag37prime-)
(Flag34prime-)
(Flag42prime-)
)
:effect
(and
(when
(and
(Flag45-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag37-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(Flag34-)
)
(and
(COLUMN0-ROBOT)
(not (NOT-COLUMN0-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN0-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN0-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(RIGHTOF3-ROBOT)
)
(and
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF2-ROBOT)
)
(and
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF2-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF1-ROBOT)
)
(and
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF0-ROBOT))
(not (RIGHTOF1-ROBOT))
)
)
(when
(and
(Flag42-)
)
(and
(LEFTOF3-ROBOT)
(not (NOT-LEFTOF3-ROBOT))
)
)
(when
(and
(LEFTOF2-ROBOT)
)
(and
(LEFTOF1-ROBOT)
(not (NOT-LEFTOF2-ROBOT))
(not (NOT-LEFTOF1-ROBOT))
)
)
(when
(and
(LEFTOF3-ROBOT)
)
(and
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF2-ROBOT))
(not (NOT-LEFTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag42prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag8prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag41prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag16prime-))
(not (Flag1prime-))
(not (Flag45prime-))
(not (Flag34prime-))
(not (Flag30prime-))
)
)
(:action MOVERIGHT-ROBOT
:parameters ()
:precondition
(and
(Flag3-)
(Flag3prime-)
(Flag37prime-)
(Flag34prime-)
(Flag30prime-)
(Flag25prime-)
(Flag24prime-)
(Flag23prime-)
(Flag22prime-)
(Flag21prime-)
)
:effect
(and
(when
(and
(Flag37-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag34-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag30-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN0-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN0-ROBOT))
)
)
(when
(and
(Flag25-)
)
(and
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(not (NOT-LEFTOF4-ROBOT))
(not (LEFTOF3-ROBOT))
)
)
(when
(and
(Flag24-)
)
(and
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(not (NOT-LEFTOF3-ROBOT))
(not (LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(not (NOT-LEFTOF2-ROBOT))
(not (LEFTOF1-ROBOT))
)
)
(when
(and
(Flag23-)
)
(and
(RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
)
)
(when
(and
(Flag22-)
)
(and
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag21-)
)
(and
(RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag42prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag8prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag41prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag16prime-))
(not (Flag1prime-))
(not (Flag45prime-))
(not (Flag34prime-))
(not (Flag30prime-))
)
)
(:action Prim1Action-1
:parameters ()
:precondition
(and
(not (ROW1-ROBOT))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF2-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF1-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
(not (Flag42prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag8prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag41prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag16prime-))
(not (Flag1prime-))
(not (Flag45prime-))
(not (Flag34prime-))
(not (Flag30prime-))
)
)
(:action Flag3Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag2-)
(Flag2prime-)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Prim3Action-2
:parameters ()
:precondition
(and
(not (Flag2-))
(Flag2prime-)
)
:effect
(and
(Flag3prime-)
(not (Flag3-))
)

)
(:action Flag5Action-0
:parameters ()
:precondition
(and
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-1
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-2
:parameters ()
:precondition
(and
(ABOVEOF1-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Prim5Action
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
(not (ABOVEOF3-ROBOT))
(not (ABOVEOF1-ROBOT))
)
:effect
(and
(Flag5prime-)
(not (Flag5-))
)

)
(:action Flag6Action-0
:parameters ()
:precondition
(and
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-1
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Prim6Action
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag6prime-)
(not (Flag6-))
)

)
(:action Flag7Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Prim7Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag7prime-)
(not (Flag7-))
)

)
(:action Flag8Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Prim8Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag8prime-)
(not (Flag8-))
)

)
(:action Flag9Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag10Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF1-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag11Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag12Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Prim9Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag9prime-)
(not (Flag9-))
)

)
(:action Prim9Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag9prime-)
(not (Flag9-))
)

)
(:action Prim10Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action Prim10Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF1-ROBOT))
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action Prim11Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag11prime-)
(not (Flag11-))
)

)
(:action Prim11Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag11prime-)
(not (Flag11-))
)

)
(:action Flag13Action
:parameters ()
:precondition
(and
(ABOVEOF2-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag14Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag15Action
:parameters ()
:precondition
(and
(ABOVEOF1-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Prim13Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag13prime-)
(not (Flag13-))
)

)
(:action Prim13Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag13prime-)
(not (Flag13-))
)

)
(:action Prim14Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag14prime-)
(not (Flag14-))
)

)
(:action Prim14Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag14prime-)
(not (Flag14-))
)

)
(:action Prim15Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF1-ROBOT))
)
:effect
(and
(Flag15prime-)
(not (Flag15-))
)

)
(:action Prim15Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag15prime-)
(not (Flag15-))
)

)
(:action Flag17Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag18Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag19Action
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Prim17Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag17prime-)
(not (Flag17-))
)

)
(:action Prim18Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag18prime-)
(not (Flag18-))
)

)
(:action Prim19Action-0
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag19prime-)
(not (Flag19-))
)

)
(:action Prim19Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag19prime-)
(not (Flag19-))
)

)
(:action Flag37Action-5
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag37-)
(Flag37prime-)
)

)
(:action Flag38Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag38Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag38Action-3
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Prim38Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag38prime-)
(not (Flag38-))
)

)
(:action Flag39Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag39-)
(Flag39prime-)
)

)
(:action Flag40Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Prim39Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim39Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim40Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag40prime-)
(not (Flag40-))
)

)
(:action MOVEDOWN-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
(Flag4prime-)
(Flag41prime-)
(Flag20prime-)
(Flag16prime-)
(Flag38prime-)
)
:effect
(and
(when
(and
(Flag41-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag20-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(Flag16-)
)
(and
(ROW0-ROBOT)
(not (NOT-ROW0-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW0-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW0-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ABOVEOF3-ROBOT)
)
(and
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF3-ROBOT))
)
)
(when
(and
(ABOVEOF2-ROBOT)
)
(and
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF1-ROBOT)
)
(and
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF1-ROBOT))
)
)
(when
(and
(Flag38-)
)
(and
(BELOWOF3-ROBOT)
(not (NOT-BELOWOF3-ROBOT))
)
)
(when
(and
(BELOWOF3-ROBOT)
)
(and
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF2-ROBOT))
(not (NOT-BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF2-ROBOT)
)
(and
(BELOWOF1-ROBOT)
(not (NOT-BELOWOF2-ROBOT))
(not (NOT-BELOWOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag42prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag8prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag41prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag16prime-))
(not (Flag1prime-))
(not (Flag45prime-))
(not (Flag34prime-))
(not (Flag30prime-))
)
)
(:action MOVEUP-ROBOT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag20prime-)
(Flag16prime-)
(Flag12prime-)
(Flag8prime-)
(Flag7prime-)
(Flag6prime-)
(Flag5prime-)
)
:effect
(and
(when
(and
(Flag20-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag16-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag12-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ROW0-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW0-ROBOT))
)
)
(when
(and
(Flag8-)
)
(and
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
(not (BELOWOF3-ROBOT))
)
)
(when
(and
(Flag7-)
)
(and
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(not (NOT-BELOWOF3-ROBOT))
(not (BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(not (NOT-BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
)
)
(when
(and
(Flag6-)
)
(and
(ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
)
)
(when
(and
(Flag5-)
)
(and
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
)
)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(not (NOT-ABOVEOF1-ROBOT))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag42prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag8prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag41prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag16prime-))
(not (Flag1prime-))
(not (Flag45prime-))
(not (Flag34prime-))
(not (Flag30prime-))
)
)
(:action Prim1Action-2
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Prim1Action-3
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Flag2Action-1
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-5
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Prim3Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag3prime-)
(not (Flag3-))
)

)
(:action Prim3Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag3prime-)
(not (Flag3-))
)

)
(:action Flag4Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Prim4Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Prim4Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Flag8Action-2
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Prim17Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag17prime-)
(not (Flag17-))
)

)
(:action Prim18Action-1
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag18prime-)
(not (Flag18-))
)

)
(:action Flag21Action-3
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag22Action-2
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag23Action-1
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag25Action-1
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Prim28Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag28prime-)
(not (Flag28-))
)

)
(:action Prim32Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag32prime-)
(not (Flag32-))
)

)
(:action Flag36Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag36-)
(Flag36prime-)
)

)
(:action Prim35Action-1
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim36Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag36prime-)
(not (Flag36-))
)

)
(:action Prim36Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag36prime-)
(not (Flag36-))
)

)
(:action Flag38Action-2
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Prim40Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag40prime-)
(not (Flag40-))
)

)
(:action Flag42Action-1
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Prim44Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
)
