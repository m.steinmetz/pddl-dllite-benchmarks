(define (domain grounded-SIMPLE-ADL-ROBOT)
(:requirements
:strips
)
(:predicates
(Flag59-)
(Flag56-)
(Flag35-)
(Flag34-)
(Flag33-)
(Flag32-)
(Flag10-)
(Flag4-)
(Flag2-)
(ROW4-ROBOT)
(ROW3-ROBOT)
(ROW2-ROBOT)
(ROW1-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(ROW0-ROBOT)
(BELOWOF1-ROBOT)
(Flag57-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag21-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag3-)
(ERROR-)
(COLUMN3-ROBOT)
(COLUMN2-ROBOT)
(COLUMN1-ROBOT)
(COLUMN0-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(LEFTOF1-ROBOT)
(COLUMN4-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(RIGHTOF4-ROBOT)
(Flag60-)
(Flag58-)
(Flag54-)
(Flag52-)
(Flag51-)
(Flag50-)
(Flag48-)
(Flag47-)
(Flag46-)
(Flag44-)
(Flag43-)
(Flag42-)
(Flag41-)
(Flag40-)
(Flag39-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag31-)
(Flag30-)
(Flag29-)
(Flag25-)
(Flag20-)
(Flag1-)
(Flag61-)
(Flag55-)
(Flag53-)
(Flag49-)
(Flag45-)
(NOT-COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(NOT-CHECKCONSISTENCY-)
(NOT-COLUMN0-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF0-ROBOT)
(NOT-LEFTOF1-ROBOT)
(NOT-ERROR-)
(NOT-ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(NOT-BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(NOT-ROW4-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(Flag59prime-)
(Flag56prime-)
(Flag35prime-)
(Flag34prime-)
(Flag33prime-)
(Flag32prime-)
(Flag10prime-)
(Flag4prime-)
(Flag2prime-)
(Flag57prime-)
(Flag28prime-)
(Flag27prime-)
(Flag26prime-)
(Flag24prime-)
(Flag23prime-)
(Flag22prime-)
(Flag21prime-)
(Flag19prime-)
(Flag18prime-)
(Flag17prime-)
(Flag16prime-)
(Flag15prime-)
(Flag14prime-)
(Flag13prime-)
(Flag12prime-)
(Flag11prime-)
(Flag9prime-)
(Flag8prime-)
(Flag7prime-)
(Flag6prime-)
(Flag5prime-)
(Flag3prime-)
(Flag60prime-)
(Flag58prime-)
(Flag54prime-)
(Flag52prime-)
(Flag51prime-)
(Flag50prime-)
(Flag48prime-)
(Flag47prime-)
(Flag46prime-)
(Flag44prime-)
(Flag43prime-)
(Flag42prime-)
(Flag41prime-)
(Flag40prime-)
(Flag39prime-)
(Flag38prime-)
(Flag37prime-)
(Flag36prime-)
(Flag31prime-)
(Flag30prime-)
(Flag29prime-)
(Flag25prime-)
(Flag20prime-)
(Flag1prime-)
(Flag61prime-)
(Flag55prime-)
(Flag53prime-)
(Flag49prime-)
(Flag45prime-)
)
(:action Flag45Action-0
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag45Action-1
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag45Action-2
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag45Action-3
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag45Action-4
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag45Action-5
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag45Action-6
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag45Action-7
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Prim45Action
:parameters ()
:precondition
(and
(not (Flag37-))
(Flag37prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag39-))
(Flag39prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag42-))
(Flag42prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag44-))
(Flag44prime-)
)
:effect
(and
(Flag45prime-)
(not (Flag45-))
)

)
(:action Flag49Action-0
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag49-)
(Flag49prime-)
)

)
(:action Flag49Action-1
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag49-)
(Flag49prime-)
)

)
(:action Flag49Action-2
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag49-)
(Flag49prime-)
)

)
(:action Flag49Action-3
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag49-)
(Flag49prime-)
)

)
(:action Flag49Action-4
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag49-)
(Flag49prime-)
)

)
(:action Flag49Action-5
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag49-)
(Flag49prime-)
)

)
(:action Flag49Action-6
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag49-)
(Flag49prime-)
)

)
(:action Flag49Action-7
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag49-)
(Flag49prime-)
)

)
(:action Flag49Action-8
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag49-)
(Flag49prime-)
)

)
(:action Prim49Action
:parameters ()
:precondition
(and
(not (Flag37-))
(Flag37prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag42-))
(Flag42prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag48-))
(Flag48prime-)
)
:effect
(and
(Flag49prime-)
(not (Flag49-))
)

)
(:action Flag53Action-0
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag53-)
(Flag53prime-)
)

)
(:action Flag53Action-1
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag53-)
(Flag53prime-)
)

)
(:action Flag53Action-2
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag53-)
(Flag53prime-)
)

)
(:action Flag53Action-3
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag53-)
(Flag53prime-)
)

)
(:action Flag53Action-4
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag53-)
(Flag53prime-)
)

)
(:action Flag53Action-5
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag53-)
(Flag53prime-)
)

)
(:action Flag53Action-6
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag53-)
(Flag53prime-)
)

)
(:action Flag53Action-7
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag53-)
(Flag53prime-)
)

)
(:action Prim53Action
:parameters ()
:precondition
(and
(not (Flag37-))
(Flag37prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag44-))
(Flag44prime-)
)
:effect
(and
(Flag53prime-)
(not (Flag53-))
)

)
(:action Flag55Action-0
:parameters ()
:precondition
(and
(Flag54-)
(Flag54prime-)
)
:effect
(and
(Flag55-)
(Flag55prime-)
)

)
(:action Flag55Action-1
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag55-)
(Flag55prime-)
)

)
(:action Flag55Action-2
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag55-)
(Flag55prime-)
)

)
(:action Flag55Action-3
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag55-)
(Flag55prime-)
)

)
(:action Flag55Action-4
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag55-)
(Flag55prime-)
)

)
(:action Prim55Action
:parameters ()
:precondition
(and
(not (Flag54-))
(Flag54prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag37-))
(Flag37prime-)
)
:effect
(and
(Flag55prime-)
(not (Flag55-))
)

)
(:action Flag61Action-0
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-1
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-2
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-3
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-4
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Prim61Action
:parameters ()
:precondition
(and
(not (Flag37-))
(Flag37prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag39-))
(Flag39prime-)
(not (Flag42-))
(Flag42prime-)
(not (Flag40-))
(Flag40prime-)
)
:effect
(and
(Flag61prime-)
(not (Flag61-))
)

)
(:action Flag1Action
:parameters ()
:precondition
(and
(COLUMN2-ROBOT)
(ROW1-ROBOT)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Prim1Action-0
:parameters ()
:precondition
(and
(not (COLUMN2-ROBOT))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Flag2Action-1
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-2
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-3
:parameters ()
:precondition
(and
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-4
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-5
:parameters ()
:precondition
(and
(RIGHTOF0-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-6
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-8
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-9
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Prim2Action
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF5-ROBOT))
(not (RIGHTOF1-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF0-ROBOT))
(not (LEFTOF2-ROBOT))
(not (RIGHTOF2-ROBOT))
(not (LEFTOF1-ROBOT))
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag2prime-)
(not (Flag2-))
)

)
(:action Flag15Action-0
:parameters ()
:precondition
(and
(Flag11-)
(Flag11prime-)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-2
:parameters ()
:precondition
(and
(Flag12-)
(Flag12prime-)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-3
:parameters ()
:precondition
(and
(Flag13-)
(Flag13prime-)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-4
:parameters ()
:precondition
(and
(Flag14-)
(Flag14prime-)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Prim15Action
:parameters ()
:precondition
(and
(not (Flag11-))
(Flag11prime-)
(not (BELOWOF1-ROBOT))
(not (Flag12-))
(Flag12prime-)
(not (Flag13-))
(Flag13prime-)
(not (Flag14-))
(Flag14prime-)
)
:effect
(and
(Flag15prime-)
(not (Flag15-))
)

)
(:action Flag20Action-0
:parameters ()
:precondition
(and
(Flag11-)
(Flag11prime-)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-1
:parameters ()
:precondition
(and
(Flag12-)
(Flag12prime-)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-2
:parameters ()
:precondition
(and
(Flag16-)
(Flag16prime-)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-3
:parameters ()
:precondition
(and
(Flag13-)
(Flag13prime-)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-4
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-5
:parameters ()
:precondition
(and
(Flag14-)
(Flag14prime-)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-6
:parameters ()
:precondition
(and
(Flag18-)
(Flag18prime-)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-7
:parameters ()
:precondition
(and
(Flag19-)
(Flag19prime-)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Prim20Action
:parameters ()
:precondition
(and
(not (Flag11-))
(Flag11prime-)
(not (Flag12-))
(Flag12prime-)
(not (Flag16-))
(Flag16prime-)
(not (Flag13-))
(Flag13prime-)
(not (Flag17-))
(Flag17prime-)
(not (Flag14-))
(Flag14prime-)
(not (Flag18-))
(Flag18prime-)
(not (Flag19-))
(Flag19prime-)
)
:effect
(and
(Flag20prime-)
(not (Flag20-))
)

)
(:action Flag25Action-0
:parameters ()
:precondition
(and
(Flag11-)
(Flag11prime-)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-1
:parameters ()
:precondition
(and
(Flag21-)
(Flag21prime-)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-2
:parameters ()
:precondition
(and
(Flag22-)
(Flag22prime-)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-3
:parameters ()
:precondition
(and
(Flag23-)
(Flag23prime-)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-4
:parameters ()
:precondition
(and
(Flag14-)
(Flag14prime-)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-5
:parameters ()
:precondition
(and
(Flag24-)
(Flag24prime-)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-6
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-7
:parameters ()
:precondition
(and
(Flag18-)
(Flag18prime-)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-8
:parameters ()
:precondition
(and
(Flag12-)
(Flag12prime-)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Prim25Action
:parameters ()
:precondition
(and
(not (Flag11-))
(Flag11prime-)
(not (Flag21-))
(Flag21prime-)
(not (Flag22-))
(Flag22prime-)
(not (Flag23-))
(Flag23prime-)
(not (Flag14-))
(Flag14prime-)
(not (Flag24-))
(Flag24prime-)
(not (Flag17-))
(Flag17prime-)
(not (Flag18-))
(Flag18prime-)
(not (Flag12-))
(Flag12prime-)
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Flag29Action-0
:parameters ()
:precondition
(and
(Flag12-)
(Flag12prime-)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-1
:parameters ()
:precondition
(and
(Flag26-)
(Flag26prime-)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-2
:parameters ()
:precondition
(and
(Flag23-)
(Flag23prime-)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-3
:parameters ()
:precondition
(and
(Flag14-)
(Flag14prime-)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-4
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-5
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-6
:parameters ()
:precondition
(and
(Flag18-)
(Flag18prime-)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-7
:parameters ()
:precondition
(and
(Flag28-)
(Flag28prime-)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Prim29Action
:parameters ()
:precondition
(and
(not (Flag12-))
(Flag12prime-)
(not (Flag26-))
(Flag26prime-)
(not (Flag23-))
(Flag23prime-)
(not (Flag14-))
(Flag14prime-)
(not (Flag27-))
(Flag27prime-)
(not (Flag17-))
(Flag17prime-)
(not (Flag18-))
(Flag18prime-)
(not (Flag28-))
(Flag28prime-)
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Flag30Action-0
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-1
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Prim30Action
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag30prime-)
(not (Flag30-))
)

)
(:action Flag31Action-0
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-1
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-2
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Prim31Action
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Flag32Action-0
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-1
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-3
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Prim32Action
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag32prime-)
(not (Flag32-))
)

)
(:action Flag33Action-0
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-1
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-2
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-4
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Prim33Action
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag33prime-)
(not (Flag33-))
)

)
(:action Flag34Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Flag34Action-1
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Flag34Action-2
:parameters ()
:precondition
(and
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Prim34Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF1-ROBOT))
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag34prime-)
(not (Flag34-))
)

)
(:action Flag35Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-1
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Prim35Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Flag36Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag36-)
(Flag36prime-)
)

)
(:action Flag36Action-1
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag36-)
(Flag36prime-)
)

)
(:action Prim36Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag36prime-)
(not (Flag36-))
)

)
(:action Flag37Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag37-)
(Flag37prime-)
)

)
(:action Flag38Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag39Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag39-)
(Flag39prime-)
)

)
(:action Flag40Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag41Action
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag42Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Flag43Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Flag44Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag44-)
(Flag44prime-)
)

)
(:action Prim37Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim38Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag38prime-)
(not (Flag38-))
)

)
(:action Prim38Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF1-ROBOT))
)
:effect
(and
(Flag38prime-)
(not (Flag38-))
)

)
(:action Prim39Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim39Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF1-ROBOT))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim40Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag40prime-)
(not (Flag40-))
)

)
(:action Prim40Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag40prime-)
(not (Flag40-))
)

)
(:action Prim41Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim42Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag42prime-)
(not (Flag42-))
)

)
(:action Prim43Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim44Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim44Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Flag46Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag46-)
(Flag46prime-)
)

)
(:action Flag47Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag48Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Prim46Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag46prime-)
(not (Flag46-))
)

)
(:action Prim46Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag46prime-)
(not (Flag46-))
)

)
(:action Prim47Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag47prime-)
(not (Flag47-))
)

)
(:action Prim47Action-1
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag47prime-)
(not (Flag47-))
)

)
(:action Prim48Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag48prime-)
(not (Flag48-))
)

)
(:action Flag50Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag50-)
(Flag50prime-)
)

)
(:action Flag51Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag51-)
(Flag51prime-)
)

)
(:action Flag52Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Prim50Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag50prime-)
(not (Flag50-))
)

)
(:action Prim50Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag50prime-)
(not (Flag50-))
)

)
(:action Prim51Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag51prime-)
(not (Flag51-))
)

)
(:action Prim52Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag52prime-)
(not (Flag52-))
)

)
(:action Flag54Action
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag54-)
(Flag54prime-)
)

)
(:action Prim54Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag54prime-)
(not (Flag54-))
)

)
(:action Prim54Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag54prime-)
(not (Flag54-))
)

)
(:action Flag58Action-0
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag58-)
(Flag58prime-)
)

)
(:action Flag58Action-1
:parameters ()
:precondition
(and
(Flag12-)
(Flag12prime-)
)
:effect
(and
(Flag58-)
(Flag58prime-)
)

)
(:action Flag58Action-2
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag58-)
(Flag58prime-)
)

)
(:action Flag58Action-3
:parameters ()
:precondition
(and
(Flag23-)
(Flag23prime-)
)
:effect
(and
(Flag58-)
(Flag58prime-)
)

)
(:action Flag58Action-4
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(Flag58-)
(Flag58prime-)
)

)
(:action Prim58Action
:parameters ()
:precondition
(and
(not (Flag27-))
(Flag27prime-)
(not (Flag12-))
(Flag12prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag23-))
(Flag23prime-)
(not (Flag17-))
(Flag17prime-)
)
:effect
(and
(Flag58prime-)
(not (Flag58-))
)

)
(:action Flag59Action-0
:parameters ()
:precondition
(and
(RIGHTOF0-ROBOT)
)
:effect
(and
(Flag59-)
(Flag59prime-)
)

)
(:action Flag59Action-1
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag59-)
(Flag59prime-)
)

)
(:action Flag59Action-2
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag59-)
(Flag59prime-)
)

)
(:action Flag59Action-3
:parameters ()
:precondition
(and
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag59-)
(Flag59prime-)
)

)
(:action Prim59Action
:parameters ()
:precondition
(and
(not (RIGHTOF0-ROBOT))
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF1-ROBOT))
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag59prime-)
(not (Flag59-))
)

)
(:action Flag60Action
:parameters ()
:precondition
(and
(RIGHTOF0-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Prim60Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF0-ROBOT))
)
:effect
(and
(Flag60prime-)
(not (Flag60-))
)

)
(:action Prim60Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag60prime-)
(not (Flag60-))
)

)
(:action MOVERIGHT-ROBOT
:parameters ()
:precondition
(and
(Flag3-)
(Flag3prime-)
(Flag53prime-)
(Flag49prime-)
(Flag45prime-)
(Flag61prime-)
(Flag59prime-)
)
:effect
(and
(when
(and
(Flag53-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag49-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag45-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag61-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN0-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN0-ROBOT))
)
)
(when
(and
(LEFTOF2-ROBOT)
)
(and
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(not (NOT-LEFTOF5-ROBOT))
(not (LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF4-ROBOT)
)
(and
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(not (NOT-LEFTOF5-ROBOT))
(not (LEFTOF4-ROBOT))
)
)
(when
(and
(LEFTOF3-ROBOT)
)
(and
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(not (NOT-LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(not (NOT-LEFTOF5-ROBOT))
(not (LEFTOF1-ROBOT))
)
)
(when
(and
(RIGHTOF4-ROBOT)
)
(and
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF3-ROBOT)
)
(and
(RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF2-ROBOT)
)
(and
(RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF1-ROBOT)
)
(and
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag59-)
)
(and
(RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag59prime-))
(not (Flag56prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag10prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag57prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag25prime-))
(not (Flag20prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag49prime-))
(not (Flag45prime-))
)
)
(:action MOVELEFT-ROBOT
:parameters ()
:precondition
(and
(Flag3-)
(Flag3prime-)
(Flag55prime-)
(Flag53prime-)
(Flag49prime-)
(Flag45prime-)
(Flag36prime-)
(Flag35prime-)
(Flag34prime-)
(Flag33prime-)
(Flag32prime-)
(Flag31prime-)
(Flag30prime-)
)
:effect
(and
(when
(and
(Flag55-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag53-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag49-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(Flag45-)
)
(and
(COLUMN0-ROBOT)
(not (NOT-COLUMN0-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN0-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN0-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(RIGHTOF4-ROBOT)
)
(and
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
(not (RIGHTOF4-ROBOT))
)
)
(when
(and
(Flag36-)
)
(and
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
(not (RIGHTOF3-ROBOT))
)
)
(when
(and
(Flag35-)
)
(and
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
(not (RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag34-)
)
(and
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF0-ROBOT))
(not (RIGHTOF1-ROBOT))
)
)
(when
(and
(Flag33-)
)
(and
(LEFTOF4-ROBOT)
(not (NOT-LEFTOF4-ROBOT))
)
)
(when
(and
(Flag32-)
)
(and
(LEFTOF3-ROBOT)
(not (NOT-LEFTOF3-ROBOT))
)
)
(when
(and
(Flag31-)
)
(and
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF2-ROBOT))
)
)
(when
(and
(Flag30-)
)
(and
(LEFTOF1-ROBOT)
(not (NOT-LEFTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag59prime-))
(not (Flag56prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag10prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag57prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag25prime-))
(not (Flag20prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag49prime-))
(not (Flag45prime-))
)
)
(:action Prim1Action-1
:parameters ()
:precondition
(and
(not (ROW1-ROBOT))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF2-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF2-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF1-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
(not (Flag59prime-))
(not (Flag56prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag10prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag57prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag25prime-))
(not (Flag20prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag49prime-))
(not (Flag45prime-))
)
)
(:action Flag3Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag2-)
(Flag2prime-)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Prim3Action-2
:parameters ()
:precondition
(and
(not (Flag2-))
(Flag2prime-)
)
:effect
(and
(Flag3prime-)
(not (Flag3-))
)

)
(:action Flag5Action-0
:parameters ()
:precondition
(and
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-1
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-2
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-3
:parameters ()
:precondition
(and
(ABOVEOF1-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Prim5Action
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
(not (ABOVEOF3-ROBOT))
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF1-ROBOT))
)
:effect
(and
(Flag5prime-)
(not (Flag5-))
)

)
(:action Flag6Action-0
:parameters ()
:precondition
(and
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-1
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-2
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Prim6Action
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
(not (ABOVEOF3-ROBOT))
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag6prime-)
(not (Flag6-))
)

)
(:action Flag7Action-0
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-1
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Prim7Action
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag7prime-)
(not (Flag7-))
)

)
(:action Flag8Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Prim8Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag8prime-)
(not (Flag8-))
)

)
(:action Flag9Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-2
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Prim9Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag9prime-)
(not (Flag9-))
)

)
(:action Flag10Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-2
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Prim10Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action Flag11Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag12Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag13Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF1-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag14Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag15Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Prim11Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag11prime-)
(not (Flag11-))
)

)
(:action Prim11Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag11prime-)
(not (Flag11-))
)

)
(:action Prim12Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag12prime-)
(not (Flag12-))
)

)
(:action Prim12Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag12prime-)
(not (Flag12-))
)

)
(:action Prim13Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag13prime-)
(not (Flag13-))
)

)
(:action Prim13Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF1-ROBOT))
)
:effect
(and
(Flag13prime-)
(not (Flag13-))
)

)
(:action Prim14Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag14prime-)
(not (Flag14-))
)

)
(:action Prim14Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag14prime-)
(not (Flag14-))
)

)
(:action Flag16Action
:parameters ()
:precondition
(and
(ABOVEOF2-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag17Action
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag18Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag19Action
:parameters ()
:precondition
(and
(ABOVEOF1-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Prim16Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag16prime-)
(not (Flag16-))
)

)
(:action Prim16Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag16prime-)
(not (Flag16-))
)

)
(:action Prim17Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag17prime-)
(not (Flag17-))
)

)
(:action Prim17Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag17prime-)
(not (Flag17-))
)

)
(:action Prim18Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag18prime-)
(not (Flag18-))
)

)
(:action Prim18Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag18prime-)
(not (Flag18-))
)

)
(:action Prim19Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF1-ROBOT))
)
:effect
(and
(Flag19prime-)
(not (Flag19-))
)

)
(:action Prim19Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag19prime-)
(not (Flag19-))
)

)
(:action Flag21Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag22Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag23Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag24Action
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Prim21Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Prim21Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Prim22Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag22prime-)
(not (Flag22-))
)

)
(:action Prim22Action-1
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag22prime-)
(not (Flag22-))
)

)
(:action Prim23Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag23prime-)
(not (Flag23-))
)

)
(:action Prim23Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag23prime-)
(not (Flag23-))
)

)
(:action Prim24Action-0
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag24prime-)
(not (Flag24-))
)

)
(:action Prim24Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag24prime-)
(not (Flag24-))
)

)
(:action Flag26Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag27Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag28Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Prim26Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag26prime-)
(not (Flag26-))
)

)
(:action Prim27Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim28Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag28prime-)
(not (Flag28-))
)

)
(:action Prim28Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag28prime-)
(not (Flag28-))
)

)
(:action Flag56Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag56Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag56Action-2
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag56Action-4
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Prim56Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag56prime-)
(not (Flag56-))
)

)
(:action Flag57Action
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Prim57Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag57prime-)
(not (Flag57-))
)

)
(:action Prim57Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag57prime-)
(not (Flag57-))
)

)
(:action MOVEDOWN-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
(Flag4prime-)
(Flag58prime-)
(Flag29prime-)
(Flag25prime-)
(Flag20prime-)
(Flag56prime-)
)
:effect
(and
(when
(and
(Flag58-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag29-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag25-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(Flag20-)
)
(and
(ROW0-ROBOT)
(not (NOT-ROW0-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW0-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW0-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ABOVEOF4-ROBOT)
)
(and
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF4-ROBOT))
)
)
(when
(and
(ABOVEOF3-ROBOT)
)
(and
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF3-ROBOT))
)
)
(when
(and
(ABOVEOF2-ROBOT)
)
(and
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF1-ROBOT)
)
(and
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF1-ROBOT))
)
)
(when
(and
(Flag56-)
)
(and
(BELOWOF4-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
)
)
(when
(and
(BELOWOF4-ROBOT)
)
(and
(BELOWOF3-ROBOT)
(not (NOT-BELOWOF3-ROBOT))
)
)
(when
(and
(BELOWOF3-ROBOT)
)
(and
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
(not (NOT-BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF2-ROBOT)
)
(and
(BELOWOF3-ROBOT)
(BELOWOF1-ROBOT)
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
(not (NOT-BELOWOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag59prime-))
(not (Flag56prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag10prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag57prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag25prime-))
(not (Flag20prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag49prime-))
(not (Flag45prime-))
)
)
(:action MOVEUP-ROBOT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag29prime-)
(Flag25prime-)
(Flag20prime-)
(Flag15prime-)
(Flag10prime-)
(Flag9prime-)
(Flag8prime-)
(Flag7prime-)
(Flag6prime-)
(Flag5prime-)
)
:effect
(and
(when
(and
(Flag29-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag25-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag20-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag15-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ROW0-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW0-ROBOT))
)
)
(when
(and
(Flag10-)
)
(and
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
(not (BELOWOF4-ROBOT))
)
)
(when
(and
(Flag9-)
)
(and
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
(not (BELOWOF3-ROBOT))
)
)
(when
(and
(Flag8-)
)
(and
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(not (NOT-BELOWOF3-ROBOT))
(not (BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(not (NOT-BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
)
)
(when
(and
(Flag7-)
)
(and
(ABOVEOF4-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
)
)
(when
(and
(Flag6-)
)
(and
(ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
)
)
(when
(and
(Flag5-)
)
(and
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
)
)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(not (NOT-ABOVEOF1-ROBOT))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag59prime-))
(not (Flag56prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag10prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag57prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag25prime-))
(not (Flag20prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag49prime-))
(not (Flag45prime-))
)
)
(:action Prim1Action-2
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Prim1Action-3
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Flag2Action-0
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-7
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Prim3Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag3prime-)
(not (Flag3-))
)

)
(:action Prim3Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag3prime-)
(not (Flag3-))
)

)
(:action Flag4Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Prim4Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Prim4Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Flag10Action-3
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Prim26Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag26prime-)
(not (Flag26-))
)

)
(:action Prim27Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Flag32Action-2
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag33Action-3
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag34Action-3
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Flag35Action-2
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Prim41Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim42Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag42prime-)
(not (Flag42-))
)

)
(:action Prim48Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag48prime-)
(not (Flag48-))
)

)
(:action Prim51Action-1
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag51prime-)
(not (Flag51-))
)

)
(:action Prim52Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag52prime-)
(not (Flag52-))
)

)
(:action Flag56Action-3
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag59Action-4
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag59-)
(Flag59prime-)
)

)
)
