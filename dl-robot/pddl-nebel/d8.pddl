(define (domain grounded-SIMPLE-ADL-ROBOT)
(:requirements
:strips
)
(:predicates
(Flag124-)
(Flag78-)
(Flag68-)
(Flag67-)
(Flag66-)
(Flag62-)
(Flag16-)
(Flag4-)
(Flag2-)
(ROW7-ROBOT)
(ROW6-ROBOT)
(ROW5-ROBOT)
(ROW4-ROBOT)
(ROW3-ROBOT)
(ROW2-ROBOT)
(ROW1-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(ROW0-ROBOT)
(BELOWOF1-ROBOT)
(Flag64-)
(Flag63-)
(Flag60-)
(Flag59-)
(Flag58-)
(Flag56-)
(Flag55-)
(Flag54-)
(Flag53-)
(Flag51-)
(Flag50-)
(Flag49-)
(Flag48-)
(Flag46-)
(Flag45-)
(Flag44-)
(Flag43-)
(Flag42-)
(Flag41-)
(Flag39-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag35-)
(Flag34-)
(Flag33-)
(Flag31-)
(Flag30-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag21-)
(Flag20-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag3-)
(ERROR-)
(COLUMN7-ROBOT)
(COLUMN6-ROBOT)
(COLUMN5-ROBOT)
(COLUMN4-ROBOT)
(COLUMN3-ROBOT)
(COLUMN2-ROBOT)
(COLUMN1-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(COLUMN0-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(LEFTOF1-ROBOT)
(Flag126-)
(Flag125-)
(Flag122-)
(Flag121-)
(Flag120-)
(Flag118-)
(Flag117-)
(Flag116-)
(Flag114-)
(Flag113-)
(Flag112-)
(Flag111-)
(Flag109-)
(Flag108-)
(Flag107-)
(Flag106-)
(Flag105-)
(Flag104-)
(Flag102-)
(Flag101-)
(Flag100-)
(Flag99-)
(Flag98-)
(Flag97-)
(Flag95-)
(Flag94-)
(Flag93-)
(Flag92-)
(Flag91-)
(Flag90-)
(Flag89-)
(Flag88-)
(Flag86-)
(Flag85-)
(Flag84-)
(Flag83-)
(Flag82-)
(Flag81-)
(Flag80-)
(Flag79-)
(Flag77-)
(Flag76-)
(Flag75-)
(Flag74-)
(Flag73-)
(Flag72-)
(Flag71-)
(Flag70-)
(Flag69-)
(Flag65-)
(Flag61-)
(Flag57-)
(Flag52-)
(Flag47-)
(Flag40-)
(Flag32-)
(Flag1-)
(Flag127-)
(Flag123-)
(Flag119-)
(Flag115-)
(Flag110-)
(Flag103-)
(Flag96-)
(Flag87-)
(NOT-COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF0-ROBOT)
(NOT-LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(NOT-CHECKCONSISTENCY-)
(NOT-COLUMN7-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(NOT-ERROR-)
(NOT-ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(NOT-BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(NOT-ROW7-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(Flag124prime-)
(Flag78prime-)
(Flag68prime-)
(Flag67prime-)
(Flag66prime-)
(Flag62prime-)
(Flag16prime-)
(Flag4prime-)
(Flag2prime-)
(Flag64prime-)
(Flag63prime-)
(Flag60prime-)
(Flag59prime-)
(Flag58prime-)
(Flag56prime-)
(Flag55prime-)
(Flag54prime-)
(Flag53prime-)
(Flag51prime-)
(Flag50prime-)
(Flag49prime-)
(Flag48prime-)
(Flag46prime-)
(Flag45prime-)
(Flag44prime-)
(Flag43prime-)
(Flag42prime-)
(Flag41prime-)
(Flag39prime-)
(Flag38prime-)
(Flag37prime-)
(Flag36prime-)
(Flag35prime-)
(Flag34prime-)
(Flag33prime-)
(Flag31prime-)
(Flag30prime-)
(Flag29prime-)
(Flag28prime-)
(Flag27prime-)
(Flag26prime-)
(Flag25prime-)
(Flag24prime-)
(Flag23prime-)
(Flag22prime-)
(Flag21prime-)
(Flag20prime-)
(Flag19prime-)
(Flag18prime-)
(Flag17prime-)
(Flag15prime-)
(Flag14prime-)
(Flag13prime-)
(Flag12prime-)
(Flag11prime-)
(Flag10prime-)
(Flag9prime-)
(Flag8prime-)
(Flag7prime-)
(Flag6prime-)
(Flag5prime-)
(Flag3prime-)
(Flag126prime-)
(Flag125prime-)
(Flag122prime-)
(Flag121prime-)
(Flag120prime-)
(Flag118prime-)
(Flag117prime-)
(Flag116prime-)
(Flag114prime-)
(Flag113prime-)
(Flag112prime-)
(Flag111prime-)
(Flag109prime-)
(Flag108prime-)
(Flag107prime-)
(Flag106prime-)
(Flag105prime-)
(Flag104prime-)
(Flag102prime-)
(Flag101prime-)
(Flag100prime-)
(Flag99prime-)
(Flag98prime-)
(Flag97prime-)
(Flag95prime-)
(Flag94prime-)
(Flag93prime-)
(Flag92prime-)
(Flag91prime-)
(Flag90prime-)
(Flag89prime-)
(Flag88prime-)
(Flag86prime-)
(Flag85prime-)
(Flag84prime-)
(Flag83prime-)
(Flag82prime-)
(Flag81prime-)
(Flag80prime-)
(Flag79prime-)
(Flag77prime-)
(Flag76prime-)
(Flag75prime-)
(Flag74prime-)
(Flag73prime-)
(Flag72prime-)
(Flag71prime-)
(Flag70prime-)
(Flag69prime-)
(Flag65prime-)
(Flag61prime-)
(Flag57prime-)
(Flag52prime-)
(Flag47prime-)
(Flag40prime-)
(Flag32prime-)
(Flag1prime-)
(Flag127prime-)
(Flag123prime-)
(Flag119prime-)
(Flag115prime-)
(Flag110prime-)
(Flag103prime-)
(Flag96prime-)
(Flag87prime-)
)
(:action Flag87Action-0
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag87-)
(Flag87prime-)
)

)
(:action Flag87Action-1
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(Flag87-)
(Flag87prime-)
)

)
(:action Flag87Action-2
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag87-)
(Flag87prime-)
)

)
(:action Flag87Action-3
:parameters ()
:precondition
(and
(Flag82-)
(Flag82prime-)
)
:effect
(and
(Flag87-)
(Flag87prime-)
)

)
(:action Flag87Action-4
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag87-)
(Flag87prime-)
)

)
(:action Flag87Action-5
:parameters ()
:precondition
(and
(Flag84-)
(Flag84prime-)
)
:effect
(and
(Flag87-)
(Flag87prime-)
)

)
(:action Flag87Action-6
:parameters ()
:precondition
(and
(Flag85-)
(Flag85prime-)
)
:effect
(and
(Flag87-)
(Flag87prime-)
)

)
(:action Flag87Action-7
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag87-)
(Flag87prime-)
)

)
(:action Prim87Action
:parameters ()
:precondition
(and
(not (Flag79-))
(Flag79prime-)
(not (Flag80-))
(Flag80prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag82-))
(Flag82prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag84-))
(Flag84prime-)
(not (Flag85-))
(Flag85prime-)
(not (Flag86-))
(Flag86prime-)
)
:effect
(and
(Flag87prime-)
(not (Flag87-))
)

)
(:action Flag96Action-0
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag96Action-1
:parameters ()
:precondition
(and
(Flag88-)
(Flag88prime-)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag96Action-2
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag96Action-3
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag96Action-4
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag96Action-5
:parameters ()
:precondition
(and
(Flag82-)
(Flag82prime-)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag96Action-6
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag96Action-7
:parameters ()
:precondition
(and
(Flag91-)
(Flag91prime-)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag96Action-8
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag96Action-9
:parameters ()
:precondition
(and
(Flag84-)
(Flag84prime-)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag96Action-10
:parameters ()
:precondition
(and
(Flag85-)
(Flag85prime-)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag96Action-11
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag96Action-12
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag96Action-13
:parameters ()
:precondition
(and
(Flag94-)
(Flag94prime-)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag96Action-14
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Prim96Action
:parameters ()
:precondition
(and
(not (Flag79-))
(Flag79prime-)
(not (Flag88-))
(Flag88prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag82-))
(Flag82prime-)
(not (Flag80-))
(Flag80prime-)
(not (Flag91-))
(Flag91prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag84-))
(Flag84prime-)
(not (Flag85-))
(Flag85prime-)
(not (Flag93-))
(Flag93prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag94-))
(Flag94prime-)
(not (Flag95-))
(Flag95prime-)
)
:effect
(and
(Flag96prime-)
(not (Flag96-))
)

)
(:action Flag103Action-0
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag103Action-1
:parameters ()
:precondition
(and
(Flag84-)
(Flag84prime-)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag103Action-2
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag103Action-3
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag103Action-4
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag103Action-5
:parameters ()
:precondition
(and
(Flag82-)
(Flag82prime-)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag103Action-6
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag103Action-7
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag103Action-8
:parameters ()
:precondition
(and
(Flag91-)
(Flag91prime-)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag103Action-9
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag103Action-10
:parameters ()
:precondition
(and
(Flag99-)
(Flag99prime-)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag103Action-11
:parameters ()
:precondition
(and
(Flag85-)
(Flag85prime-)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag103Action-12
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag103Action-13
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag103Action-14
:parameters ()
:precondition
(and
(Flag101-)
(Flag101prime-)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag103Action-15
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag103Action-16
:parameters ()
:precondition
(and
(Flag94-)
(Flag94prime-)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag103Action-17
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag103Action-18
:parameters ()
:precondition
(and
(Flag102-)
(Flag102prime-)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Prim103Action
:parameters ()
:precondition
(and
(not (Flag79-))
(Flag79prime-)
(not (Flag84-))
(Flag84prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag80-))
(Flag80prime-)
(not (Flag82-))
(Flag82prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag91-))
(Flag91prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag99-))
(Flag99prime-)
(not (Flag85-))
(Flag85prime-)
(not (Flag100-))
(Flag100prime-)
(not (Flag93-))
(Flag93prime-)
(not (Flag101-))
(Flag101prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag94-))
(Flag94prime-)
(not (Flag95-))
(Flag95prime-)
(not (Flag102-))
(Flag102prime-)
)
:effect
(and
(Flag103prime-)
(not (Flag103-))
)

)
(:action Flag110Action-0
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag110-)
(Flag110prime-)
)

)
(:action Flag110Action-1
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag110-)
(Flag110prime-)
)

)
(:action Flag110Action-2
:parameters ()
:precondition
(and
(Flag105-)
(Flag105prime-)
)
:effect
(and
(Flag110-)
(Flag110prime-)
)

)
(:action Flag110Action-3
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag110-)
(Flag110prime-)
)

)
(:action Flag110Action-4
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag110-)
(Flag110prime-)
)

)
(:action Flag110Action-5
:parameters ()
:precondition
(and
(Flag82-)
(Flag82prime-)
)
:effect
(and
(Flag110-)
(Flag110prime-)
)

)
(:action Flag110Action-6
:parameters ()
:precondition
(and
(Flag106-)
(Flag106prime-)
)
:effect
(and
(Flag110-)
(Flag110prime-)
)

)
(:action Flag110Action-7
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag110-)
(Flag110prime-)
)

)
(:action Flag110Action-8
:parameters ()
:precondition
(and
(Flag107-)
(Flag107prime-)
)
:effect
(and
(Flag110-)
(Flag110prime-)
)

)
(:action Flag110Action-9
:parameters ()
:precondition
(and
(Flag99-)
(Flag99prime-)
)
:effect
(and
(Flag110-)
(Flag110prime-)
)

)
(:action Flag110Action-10
:parameters ()
:precondition
(and
(Flag91-)
(Flag91prime-)
)
:effect
(and
(Flag110-)
(Flag110prime-)
)

)
(:action Flag110Action-11
:parameters ()
:precondition
(and
(Flag84-)
(Flag84prime-)
)
:effect
(and
(Flag110-)
(Flag110prime-)
)

)
(:action Flag110Action-12
:parameters ()
:precondition
(and
(Flag101-)
(Flag101prime-)
)
:effect
(and
(Flag110-)
(Flag110prime-)
)

)
(:action Flag110Action-13
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(Flag110-)
(Flag110prime-)
)

)
(:action Flag110Action-14
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(Flag110-)
(Flag110prime-)
)

)
(:action Flag110Action-15
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag110-)
(Flag110prime-)
)

)
(:action Flag110Action-16
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag110-)
(Flag110prime-)
)

)
(:action Flag110Action-17
:parameters ()
:precondition
(and
(Flag94-)
(Flag94prime-)
)
:effect
(and
(Flag110-)
(Flag110prime-)
)

)
(:action Flag110Action-18
:parameters ()
:precondition
(and
(Flag109-)
(Flag109prime-)
)
:effect
(and
(Flag110-)
(Flag110prime-)
)

)
(:action Flag110Action-19
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(Flag110-)
(Flag110prime-)
)

)
(:action Flag110Action-20
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(Flag110-)
(Flag110prime-)
)

)
(:action Prim110Action
:parameters ()
:precondition
(and
(not (Flag79-))
(Flag79prime-)
(not (Flag104-))
(Flag104prime-)
(not (Flag105-))
(Flag105prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag82-))
(Flag82prime-)
(not (Flag106-))
(Flag106prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag107-))
(Flag107prime-)
(not (Flag99-))
(Flag99prime-)
(not (Flag91-))
(Flag91prime-)
(not (Flag84-))
(Flag84prime-)
(not (Flag101-))
(Flag101prime-)
(not (Flag100-))
(Flag100prime-)
(not (Flag93-))
(Flag93prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag94-))
(Flag94prime-)
(not (Flag109-))
(Flag109prime-)
(not (Flag95-))
(Flag95prime-)
(not (Flag80-))
(Flag80prime-)
)
:effect
(and
(Flag110prime-)
(not (Flag110-))
)

)
(:action Flag115Action-0
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag115-)
(Flag115prime-)
)

)
(:action Flag115Action-1
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag115-)
(Flag115prime-)
)

)
(:action Flag115Action-2
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag115-)
(Flag115prime-)
)

)
(:action Flag115Action-3
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag115-)
(Flag115prime-)
)

)
(:action Flag115Action-4
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag115-)
(Flag115prime-)
)

)
(:action Flag115Action-5
:parameters ()
:precondition
(and
(Flag106-)
(Flag106prime-)
)
:effect
(and
(Flag115-)
(Flag115prime-)
)

)
(:action Flag115Action-6
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag115-)
(Flag115prime-)
)

)
(:action Flag115Action-7
:parameters ()
:precondition
(and
(Flag107-)
(Flag107prime-)
)
:effect
(and
(Flag115-)
(Flag115prime-)
)

)
(:action Flag115Action-8
:parameters ()
:precondition
(and
(Flag99-)
(Flag99prime-)
)
:effect
(and
(Flag115-)
(Flag115prime-)
)

)
(:action Flag115Action-9
:parameters ()
:precondition
(and
(Flag91-)
(Flag91prime-)
)
:effect
(and
(Flag115-)
(Flag115prime-)
)

)
(:action Flag115Action-10
:parameters ()
:precondition
(and
(Flag84-)
(Flag84prime-)
)
:effect
(and
(Flag115-)
(Flag115prime-)
)

)
(:action Flag115Action-11
:parameters ()
:precondition
(and
(Flag101-)
(Flag101prime-)
)
:effect
(and
(Flag115-)
(Flag115prime-)
)

)
(:action Flag115Action-12
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(Flag115-)
(Flag115prime-)
)

)
(:action Flag115Action-13
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(Flag115-)
(Flag115prime-)
)

)
(:action Flag115Action-14
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag115-)
(Flag115prime-)
)

)
(:action Flag115Action-15
:parameters ()
:precondition
(and
(Flag112-)
(Flag112prime-)
)
:effect
(and
(Flag115-)
(Flag115prime-)
)

)
(:action Flag115Action-16
:parameters ()
:precondition
(and
(Flag113-)
(Flag113prime-)
)
:effect
(and
(Flag115-)
(Flag115prime-)
)

)
(:action Flag115Action-17
:parameters ()
:precondition
(and
(Flag94-)
(Flag94prime-)
)
:effect
(and
(Flag115-)
(Flag115prime-)
)

)
(:action Flag115Action-18
:parameters ()
:precondition
(and
(Flag109-)
(Flag109prime-)
)
:effect
(and
(Flag115-)
(Flag115prime-)
)

)
(:action Flag115Action-19
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(Flag115-)
(Flag115prime-)
)

)
(:action Flag115Action-20
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag115-)
(Flag115prime-)
)

)
(:action Prim115Action
:parameters ()
:precondition
(and
(not (Flag79-))
(Flag79prime-)
(not (Flag104-))
(Flag104prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag106-))
(Flag106prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag107-))
(Flag107prime-)
(not (Flag99-))
(Flag99prime-)
(not (Flag91-))
(Flag91prime-)
(not (Flag84-))
(Flag84prime-)
(not (Flag101-))
(Flag101prime-)
(not (Flag100-))
(Flag100prime-)
(not (Flag80-))
(Flag80prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag112-))
(Flag112prime-)
(not (Flag113-))
(Flag113prime-)
(not (Flag94-))
(Flag94prime-)
(not (Flag109-))
(Flag109prime-)
(not (Flag95-))
(Flag95prime-)
(not (Flag114-))
(Flag114prime-)
)
:effect
(and
(Flag115prime-)
(not (Flag115-))
)

)
(:action Flag119Action-0
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag119-)
(Flag119prime-)
)

)
(:action Flag119Action-1
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag119-)
(Flag119prime-)
)

)
(:action Flag119Action-2
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag119-)
(Flag119prime-)
)

)
(:action Flag119Action-3
:parameters ()
:precondition
(and
(Flag106-)
(Flag106prime-)
)
:effect
(and
(Flag119-)
(Flag119prime-)
)

)
(:action Flag119Action-4
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag119-)
(Flag119prime-)
)

)
(:action Flag119Action-5
:parameters ()
:precondition
(and
(Flag107-)
(Flag107prime-)
)
:effect
(and
(Flag119-)
(Flag119prime-)
)

)
(:action Flag119Action-6
:parameters ()
:precondition
(and
(Flag91-)
(Flag91prime-)
)
:effect
(and
(Flag119-)
(Flag119prime-)
)

)
(:action Flag119Action-7
:parameters ()
:precondition
(and
(Flag99-)
(Flag99prime-)
)
:effect
(and
(Flag119-)
(Flag119prime-)
)

)
(:action Flag119Action-8
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(Flag119-)
(Flag119prime-)
)

)
(:action Flag119Action-9
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(Flag119-)
(Flag119prime-)
)

)
(:action Flag119Action-10
:parameters ()
:precondition
(and
(Flag101-)
(Flag101prime-)
)
:effect
(and
(Flag119-)
(Flag119prime-)
)

)
(:action Flag119Action-11
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag119-)
(Flag119prime-)
)

)
(:action Flag119Action-12
:parameters ()
:precondition
(and
(Flag112-)
(Flag112prime-)
)
:effect
(and
(Flag119-)
(Flag119prime-)
)

)
(:action Flag119Action-13
:parameters ()
:precondition
(and
(Flag117-)
(Flag117prime-)
)
:effect
(and
(Flag119-)
(Flag119prime-)
)

)
(:action Flag119Action-14
:parameters ()
:precondition
(and
(Flag94-)
(Flag94prime-)
)
:effect
(and
(Flag119-)
(Flag119prime-)
)

)
(:action Flag119Action-15
:parameters ()
:precondition
(and
(Flag118-)
(Flag118prime-)
)
:effect
(and
(Flag119-)
(Flag119prime-)
)

)
(:action Flag119Action-16
:parameters ()
:precondition
(and
(Flag113-)
(Flag113prime-)
)
:effect
(and
(Flag119-)
(Flag119prime-)
)

)
(:action Flag119Action-17
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag119-)
(Flag119prime-)
)

)
(:action Prim119Action
:parameters ()
:precondition
(and
(not (Flag104-))
(Flag104prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag106-))
(Flag106prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag107-))
(Flag107prime-)
(not (Flag91-))
(Flag91prime-)
(not (Flag99-))
(Flag99prime-)
(not (Flag116-))
(Flag116prime-)
(not (Flag80-))
(Flag80prime-)
(not (Flag101-))
(Flag101prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag112-))
(Flag112prime-)
(not (Flag117-))
(Flag117prime-)
(not (Flag94-))
(Flag94prime-)
(not (Flag118-))
(Flag118prime-)
(not (Flag113-))
(Flag113prime-)
(not (Flag114-))
(Flag114prime-)
)
:effect
(and
(Flag119prime-)
(not (Flag119-))
)

)
(:action Flag123Action-0
:parameters ()
:precondition
(and
(Flag120-)
(Flag120prime-)
)
:effect
(and
(Flag123-)
(Flag123prime-)
)

)
(:action Flag123Action-1
:parameters ()
:precondition
(and
(Flag121-)
(Flag121prime-)
)
:effect
(and
(Flag123-)
(Flag123prime-)
)

)
(:action Flag123Action-2
:parameters ()
:precondition
(and
(Flag118-)
(Flag118prime-)
)
:effect
(and
(Flag123-)
(Flag123prime-)
)

)
(:action Flag123Action-3
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag123-)
(Flag123prime-)
)

)
(:action Flag123Action-4
:parameters ()
:precondition
(and
(Flag113-)
(Flag113prime-)
)
:effect
(and
(Flag123-)
(Flag123prime-)
)

)
(:action Flag123Action-5
:parameters ()
:precondition
(and
(Flag106-)
(Flag106prime-)
)
:effect
(and
(Flag123-)
(Flag123prime-)
)

)
(:action Flag123Action-6
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(Flag123-)
(Flag123prime-)
)

)
(:action Flag123Action-7
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag123-)
(Flag123prime-)
)

)
(:action Flag123Action-8
:parameters ()
:precondition
(and
(Flag122-)
(Flag122prime-)
)
:effect
(and
(Flag123-)
(Flag123prime-)
)

)
(:action Flag123Action-9
:parameters ()
:precondition
(and
(Flag101-)
(Flag101prime-)
)
:effect
(and
(Flag123-)
(Flag123prime-)
)

)
(:action Flag123Action-10
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag123-)
(Flag123prime-)
)

)
(:action Flag123Action-11
:parameters ()
:precondition
(and
(Flag94-)
(Flag94prime-)
)
:effect
(and
(Flag123-)
(Flag123prime-)
)

)
(:action Flag123Action-12
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag123-)
(Flag123prime-)
)

)
(:action Flag123Action-13
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag123-)
(Flag123prime-)
)

)
(:action Prim123Action
:parameters ()
:precondition
(and
(not (Flag120-))
(Flag120prime-)
(not (Flag121-))
(Flag121prime-)
(not (Flag118-))
(Flag118prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag113-))
(Flag113prime-)
(not (Flag106-))
(Flag106prime-)
(not (Flag80-))
(Flag80prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag122-))
(Flag122prime-)
(not (Flag101-))
(Flag101prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag94-))
(Flag94prime-)
(not (Flag104-))
(Flag104prime-)
(not (Flag114-))
(Flag114prime-)
)
:effect
(and
(Flag123prime-)
(not (Flag123-))
)

)
(:action Flag127Action-0
:parameters ()
:precondition
(and
(Flag125-)
(Flag125prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-1
:parameters ()
:precondition
(and
(Flag118-)
(Flag118prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-2
:parameters ()
:precondition
(and
(Flag126-)
(Flag126prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-3
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-4
:parameters ()
:precondition
(and
(Flag113-)
(Flag113prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-5
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-6
:parameters ()
:precondition
(and
(Flag101-)
(Flag101prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-7
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Prim127Action
:parameters ()
:precondition
(and
(not (Flag125-))
(Flag125prime-)
(not (Flag118-))
(Flag118prime-)
(not (Flag126-))
(Flag126prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag113-))
(Flag113prime-)
(not (Flag80-))
(Flag80prime-)
(not (Flag101-))
(Flag101prime-)
(not (Flag104-))
(Flag104prime-)
)
:effect
(and
(Flag127prime-)
(not (Flag127-))
)

)
(:action Flag1Action
:parameters ()
:precondition
(and
(COLUMN2-ROBOT)
(ROW1-ROBOT)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Prim1Action-0
:parameters ()
:precondition
(and
(not (COLUMN2-ROBOT))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Flag2Action-0
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-1
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-2
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-3
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-4
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-5
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-6
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-7
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-8
:parameters ()
:precondition
(and
(RIGHTOF0-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-10
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-12
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-13
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-14
:parameters ()
:precondition
(and
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-15
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Prim2Action
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
(not (RIGHTOF6-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF3-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF6-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF0-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF2-ROBOT))
(not (RIGHTOF2-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (LEFTOF1-ROBOT))
(not (RIGHTOF1-ROBOT))
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag2prime-)
(not (Flag2-))
)

)
(:action Flag24Action-0
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-1
:parameters ()
:precondition
(and
(Flag18-)
(Flag18prime-)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-2
:parameters ()
:precondition
(and
(Flag19-)
(Flag19prime-)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-3
:parameters ()
:precondition
(and
(Flag20-)
(Flag20prime-)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-4
:parameters ()
:precondition
(and
(Flag21-)
(Flag21prime-)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-5
:parameters ()
:precondition
(and
(Flag22-)
(Flag22prime-)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-7
:parameters ()
:precondition
(and
(Flag23-)
(Flag23prime-)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Prim24Action
:parameters ()
:precondition
(and
(not (Flag17-))
(Flag17prime-)
(not (Flag18-))
(Flag18prime-)
(not (Flag19-))
(Flag19prime-)
(not (Flag20-))
(Flag20prime-)
(not (Flag21-))
(Flag21prime-)
(not (Flag22-))
(Flag22prime-)
(not (BELOWOF1-ROBOT))
(not (Flag23-))
(Flag23prime-)
)
:effect
(and
(Flag24prime-)
(not (Flag24-))
)

)
(:action Flag32Action-0
:parameters ()
:precondition
(and
(Flag18-)
(Flag18prime-)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-1
:parameters ()
:precondition
(and
(Flag25-)
(Flag25prime-)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-2
:parameters ()
:precondition
(and
(Flag26-)
(Flag26prime-)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-3
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-4
:parameters ()
:precondition
(and
(Flag21-)
(Flag21prime-)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-5
:parameters ()
:precondition
(and
(Flag20-)
(Flag20prime-)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-6
:parameters ()
:precondition
(and
(Flag19-)
(Flag19prime-)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-7
:parameters ()
:precondition
(and
(Flag22-)
(Flag22prime-)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-8
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-9
:parameters ()
:precondition
(and
(Flag28-)
(Flag28prime-)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-10
:parameters ()
:precondition
(and
(Flag23-)
(Flag23prime-)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-11
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-12
:parameters ()
:precondition
(and
(Flag30-)
(Flag30prime-)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-13
:parameters ()
:precondition
(and
(Flag31-)
(Flag31prime-)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Prim32Action
:parameters ()
:precondition
(and
(not (Flag18-))
(Flag18prime-)
(not (Flag25-))
(Flag25prime-)
(not (Flag26-))
(Flag26prime-)
(not (Flag17-))
(Flag17prime-)
(not (Flag21-))
(Flag21prime-)
(not (Flag20-))
(Flag20prime-)
(not (Flag19-))
(Flag19prime-)
(not (Flag22-))
(Flag22prime-)
(not (Flag27-))
(Flag27prime-)
(not (Flag28-))
(Flag28prime-)
(not (Flag23-))
(Flag23prime-)
(not (Flag29-))
(Flag29prime-)
(not (Flag30-))
(Flag30prime-)
(not (Flag31-))
(Flag31prime-)
)
:effect
(and
(Flag32prime-)
(not (Flag32-))
)

)
(:action Flag40Action-0
:parameters ()
:precondition
(and
(Flag18-)
(Flag18prime-)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag40Action-1
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag40Action-2
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag40Action-3
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag40Action-4
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag40Action-5
:parameters ()
:precondition
(and
(Flag20-)
(Flag20prime-)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag40Action-6
:parameters ()
:precondition
(and
(Flag19-)
(Flag19prime-)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag40Action-7
:parameters ()
:precondition
(and
(Flag22-)
(Flag22prime-)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag40Action-8
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag40Action-9
:parameters ()
:precondition
(and
(Flag23-)
(Flag23prime-)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag40Action-10
:parameters ()
:precondition
(and
(Flag25-)
(Flag25prime-)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag40Action-11
:parameters ()
:precondition
(and
(Flag28-)
(Flag28prime-)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag40Action-12
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag40Action-13
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag40Action-14
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag40Action-15
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag40Action-16
:parameters ()
:precondition
(and
(Flag30-)
(Flag30prime-)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag40Action-17
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Prim40Action
:parameters ()
:precondition
(and
(not (Flag18-))
(Flag18prime-)
(not (Flag33-))
(Flag33prime-)
(not (Flag34-))
(Flag34prime-)
(not (Flag35-))
(Flag35prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag20-))
(Flag20prime-)
(not (Flag19-))
(Flag19prime-)
(not (Flag22-))
(Flag22prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag23-))
(Flag23prime-)
(not (Flag25-))
(Flag25prime-)
(not (Flag28-))
(Flag28prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag39-))
(Flag39prime-)
(not (Flag27-))
(Flag27prime-)
(not (Flag29-))
(Flag29prime-)
(not (Flag30-))
(Flag30prime-)
(not (Flag17-))
(Flag17prime-)
)
:effect
(and
(Flag40prime-)
(not (Flag40-))
)

)
(:action Flag47Action-0
:parameters ()
:precondition
(and
(Flag23-)
(Flag23prime-)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag47Action-1
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag47Action-2
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag47Action-3
:parameters ()
:precondition
(and
(Flag22-)
(Flag22prime-)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag47Action-4
:parameters ()
:precondition
(and
(Flag19-)
(Flag19prime-)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag47Action-5
:parameters ()
:precondition
(and
(Flag20-)
(Flag20prime-)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag47Action-6
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag47Action-7
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag47Action-8
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag47Action-9
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag47Action-10
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag47Action-11
:parameters ()
:precondition
(and
(Flag28-)
(Flag28prime-)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag47Action-12
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag47Action-13
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag47Action-14
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag47Action-15
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag47Action-16
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag47Action-17
:parameters ()
:precondition
(and
(Flag25-)
(Flag25prime-)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag47Action-18
:parameters ()
:precondition
(and
(Flag30-)
(Flag30prime-)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag47Action-19
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Prim47Action
:parameters ()
:precondition
(and
(not (Flag23-))
(Flag23prime-)
(not (Flag34-))
(Flag34prime-)
(not (Flag17-))
(Flag17prime-)
(not (Flag22-))
(Flag22prime-)
(not (Flag19-))
(Flag19prime-)
(not (Flag20-))
(Flag20prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag42-))
(Flag42prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag28-))
(Flag28prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag27-))
(Flag27prime-)
(not (Flag29-))
(Flag29prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag25-))
(Flag25prime-)
(not (Flag30-))
(Flag30prime-)
(not (Flag46-))
(Flag46prime-)
)
:effect
(and
(Flag47prime-)
(not (Flag47-))
)

)
(:action Flag52Action-0
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-1
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-2
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-3
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-4
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-5
:parameters ()
:precondition
(and
(Flag20-)
(Flag20prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-6
:parameters ()
:precondition
(and
(Flag28-)
(Flag28prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-7
:parameters ()
:precondition
(and
(Flag19-)
(Flag19prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-8
:parameters ()
:precondition
(and
(Flag22-)
(Flag22prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-9
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-10
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-11
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-12
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-13
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-14
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-15
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-16
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-17
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-18
:parameters ()
:precondition
(and
(Flag25-)
(Flag25prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-19
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Prim52Action
:parameters ()
:precondition
(and
(not (Flag17-))
(Flag17prime-)
(not (Flag34-))
(Flag34prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag20-))
(Flag20prime-)
(not (Flag28-))
(Flag28prime-)
(not (Flag19-))
(Flag19prime-)
(not (Flag22-))
(Flag22prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag27-))
(Flag27prime-)
(not (Flag29-))
(Flag29prime-)
(not (Flag25-))
(Flag25prime-)
(not (Flag42-))
(Flag42prime-)
)
:effect
(and
(Flag52prime-)
(not (Flag52-))
)

)
(:action Flag57Action-0
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Flag57Action-1
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Flag57Action-2
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Flag57Action-3
:parameters ()
:precondition
(and
(Flag19-)
(Flag19prime-)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Flag57Action-4
:parameters ()
:precondition
(and
(Flag20-)
(Flag20prime-)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Flag57Action-5
:parameters ()
:precondition
(and
(Flag28-)
(Flag28prime-)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Flag57Action-6
:parameters ()
:precondition
(and
(Flag54-)
(Flag54prime-)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Flag57Action-7
:parameters ()
:precondition
(and
(Flag22-)
(Flag22prime-)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Flag57Action-8
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Flag57Action-9
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Flag57Action-10
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Flag57Action-11
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Flag57Action-12
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Flag57Action-13
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Flag57Action-14
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Flag57Action-15
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Flag57Action-16
:parameters ()
:precondition
(and
(Flag25-)
(Flag25prime-)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Flag57Action-17
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Prim57Action
:parameters ()
:precondition
(and
(not (Flag29-))
(Flag29prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag19-))
(Flag19prime-)
(not (Flag20-))
(Flag20prime-)
(not (Flag28-))
(Flag28prime-)
(not (Flag54-))
(Flag54prime-)
(not (Flag22-))
(Flag22prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag34-))
(Flag34prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag25-))
(Flag25prime-)
(not (Flag42-))
(Flag42prime-)
)
:effect
(and
(Flag57prime-)
(not (Flag57-))
)

)
(:action Flag61Action-0
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-1
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-2
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-3
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-4
:parameters ()
:precondition
(and
(Flag19-)
(Flag19prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-5
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-6
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-7
:parameters ()
:precondition
(and
(Flag25-)
(Flag25prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-8
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-9
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-10
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-11
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-12
:parameters ()
:precondition
(and
(Flag20-)
(Flag20prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-13
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Prim61Action
:parameters ()
:precondition
(and
(not (Flag29-))
(Flag29prime-)
(not (Flag34-))
(Flag34prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag19-))
(Flag19prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag25-))
(Flag25prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag20-))
(Flag20prime-)
(not (Flag60-))
(Flag60prime-)
)
:effect
(and
(Flag61prime-)
(not (Flag61-))
)

)
(:action Flag65Action-0
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(Flag65-)
(Flag65prime-)
)

)
(:action Flag65Action-1
:parameters ()
:precondition
(and
(Flag20-)
(Flag20prime-)
)
:effect
(and
(Flag65-)
(Flag65prime-)
)

)
(:action Flag65Action-2
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag65-)
(Flag65prime-)
)

)
(:action Flag65Action-3
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag65-)
(Flag65prime-)
)

)
(:action Flag65Action-4
:parameters ()
:precondition
(and
(Flag64-)
(Flag64prime-)
)
:effect
(and
(Flag65-)
(Flag65prime-)
)

)
(:action Flag65Action-5
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag65-)
(Flag65prime-)
)

)
(:action Flag65Action-6
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag65-)
(Flag65prime-)
)

)
(:action Flag65Action-7
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag65-)
(Flag65prime-)
)

)
(:action Prim65Action
:parameters ()
:precondition
(and
(not (Flag29-))
(Flag29prime-)
(not (Flag20-))
(Flag20prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag64-))
(Flag64prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag50-))
(Flag50prime-)
)
:effect
(and
(Flag65prime-)
(not (Flag65-))
)

)
(:action Flag66Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Flag66Action-1
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Flag66Action-3
:parameters ()
:precondition
(and
(RIGHTOF0-ROBOT)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Flag66Action-4
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Flag66Action-5
:parameters ()
:precondition
(and
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Flag66Action-6
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Flag66Action-7
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Prim66Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF2-ROBOT))
(not (RIGHTOF0-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF1-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag66prime-)
(not (Flag66-))
)

)
(:action Flag67Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-1
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-3
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-4
:parameters ()
:precondition
(and
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-5
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-6
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Prim67Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF2-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF1-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag67prime-)
(not (Flag67-))
)

)
(:action Flag68Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag68-)
(Flag68prime-)
)

)
(:action Flag68Action-1
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag68-)
(Flag68prime-)
)

)
(:action Flag68Action-3
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag68-)
(Flag68prime-)
)

)
(:action Flag68Action-4
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag68-)
(Flag68prime-)
)

)
(:action Flag68Action-5
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag68-)
(Flag68prime-)
)

)
(:action Prim68Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF2-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag68prime-)
(not (Flag68-))
)

)
(:action Flag69Action-0
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-1
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-2
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-3
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-4
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Prim69Action
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Flag70Action-0
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-1
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-2
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-3
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Prim70Action
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag70prime-)
(not (Flag70-))
)

)
(:action Flag71Action-0
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag71-)
(Flag71prime-)
)

)
(:action Flag71Action-1
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag71-)
(Flag71prime-)
)

)
(:action Flag71Action-2
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag71-)
(Flag71prime-)
)

)
(:action Prim71Action
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag71prime-)
(not (Flag71-))
)

)
(:action Flag72Action-0
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-1
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Prim72Action
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag72prime-)
(not (Flag72-))
)

)
(:action Flag73Action-0
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-1
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Prim73Action
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag73prime-)
(not (Flag73-))
)

)
(:action Flag74Action-0
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag74-)
(Flag74prime-)
)

)
(:action Flag74Action-1
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag74-)
(Flag74prime-)
)

)
(:action Flag74Action-2
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag74-)
(Flag74prime-)
)

)
(:action Prim74Action
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag74prime-)
(not (Flag74-))
)

)
(:action Flag75Action-0
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-1
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-2
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-3
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Prim75Action
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag75prime-)
(not (Flag75-))
)

)
(:action Flag76Action-0
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag76-)
(Flag76prime-)
)

)
(:action Flag76Action-1
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag76-)
(Flag76prime-)
)

)
(:action Flag76Action-2
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag76-)
(Flag76prime-)
)

)
(:action Flag76Action-3
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag76-)
(Flag76prime-)
)

)
(:action Flag76Action-4
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag76-)
(Flag76prime-)
)

)
(:action Prim76Action
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag76prime-)
(not (Flag76-))
)

)
(:action Flag77Action-0
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-1
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-2
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-3
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-4
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-5
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Prim77Action
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag77prime-)
(not (Flag77-))
)

)
(:action Flag78Action-0
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag78-)
(Flag78prime-)
)

)
(:action Flag78Action-2
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag78-)
(Flag78prime-)
)

)
(:action Flag78Action-3
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag78-)
(Flag78prime-)
)

)
(:action Flag78Action-4
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag78-)
(Flag78prime-)
)

)
(:action Flag78Action-5
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag78-)
(Flag78prime-)
)

)
(:action Flag78Action-6
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag78-)
(Flag78prime-)
)

)
(:action Prim78Action
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag78prime-)
(not (Flag78-))
)

)
(:action Flag79Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag79-)
(Flag79prime-)
)

)
(:action Flag80Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag80-)
(Flag80prime-)
)

)
(:action Flag81Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag82Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag83Action
:parameters ()
:precondition
(and
(RIGHTOF0-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag83-)
(Flag83prime-)
)

)
(:action Flag84Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag85Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag85-)
(Flag85prime-)
)

)
(:action Flag86Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Prim79Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag79prime-)
(not (Flag79-))
)

)
(:action Prim79Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag79prime-)
(not (Flag79-))
)

)
(:action Prim80Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag80prime-)
(not (Flag80-))
)

)
(:action Prim80Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag80prime-)
(not (Flag80-))
)

)
(:action Prim81Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag81prime-)
(not (Flag81-))
)

)
(:action Prim81Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF1-ROBOT))
)
:effect
(and
(Flag81prime-)
(not (Flag81-))
)

)
(:action Prim82Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag82prime-)
(not (Flag82-))
)

)
(:action Prim82Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag82prime-)
(not (Flag82-))
)

)
(:action Prim83Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF0-ROBOT))
)
:effect
(and
(Flag83prime-)
(not (Flag83-))
)

)
(:action Prim83Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag83prime-)
(not (Flag83-))
)

)
(:action Prim84Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag84prime-)
(not (Flag84-))
)

)
(:action Prim84Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag84prime-)
(not (Flag84-))
)

)
(:action Prim85Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag85prime-)
(not (Flag85-))
)

)
(:action Prim86Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag86prime-)
(not (Flag86-))
)

)
(:action Prim86Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag86prime-)
(not (Flag86-))
)

)
(:action Flag88Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag88-)
(Flag88prime-)
)

)
(:action Flag89Action
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag89-)
(Flag89prime-)
)

)
(:action Flag90Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag91Action
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag92Action
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag92-)
(Flag92prime-)
)

)
(:action Flag93Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag93-)
(Flag93prime-)
)

)
(:action Flag94Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag94-)
(Flag94prime-)
)

)
(:action Flag95Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Prim88Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag88prime-)
(not (Flag88-))
)

)
(:action Prim88Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF1-ROBOT))
)
:effect
(and
(Flag88prime-)
(not (Flag88-))
)

)
(:action Prim89Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag89prime-)
(not (Flag89-))
)

)
(:action Prim89Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag89prime-)
(not (Flag89-))
)

)
(:action Prim90Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim91Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag91prime-)
(not (Flag91-))
)

)
(:action Prim91Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag91prime-)
(not (Flag91-))
)

)
(:action Prim92Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag92prime-)
(not (Flag92-))
)

)
(:action Prim93Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag93prime-)
(not (Flag93-))
)

)
(:action Prim93Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag93prime-)
(not (Flag93-))
)

)
(:action Prim94Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag94prime-)
(not (Flag94-))
)

)
(:action Prim94Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag94prime-)
(not (Flag94-))
)

)
(:action Prim95Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag95prime-)
(not (Flag95-))
)

)
(:action Prim95Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag95prime-)
(not (Flag95-))
)

)
(:action Flag97Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag97-)
(Flag97prime-)
)

)
(:action Flag98Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag99Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag100Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag100-)
(Flag100prime-)
)

)
(:action Flag101Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag101-)
(Flag101prime-)
)

)
(:action Flag102Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag102-)
(Flag102prime-)
)

)
(:action Prim97Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag97prime-)
(not (Flag97-))
)

)
(:action Prim97Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag97prime-)
(not (Flag97-))
)

)
(:action Prim98Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag98prime-)
(not (Flag98-))
)

)
(:action Prim98Action-1
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag98prime-)
(not (Flag98-))
)

)
(:action Prim99Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag99prime-)
(not (Flag99-))
)

)
(:action Prim99Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag99prime-)
(not (Flag99-))
)

)
(:action Prim100Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag100prime-)
(not (Flag100-))
)

)
(:action Prim100Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag100prime-)
(not (Flag100-))
)

)
(:action Prim101Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag101prime-)
(not (Flag101-))
)

)
(:action Prim101Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag101prime-)
(not (Flag101-))
)

)
(:action Prim102Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag102prime-)
(not (Flag102-))
)

)
(:action Flag104Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag104-)
(Flag104prime-)
)

)
(:action Flag105Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag105-)
(Flag105prime-)
)

)
(:action Flag106Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag107Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag108Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag108-)
(Flag108prime-)
)

)
(:action Flag109Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Prim104Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag104prime-)
(not (Flag104-))
)

)
(:action Prim104Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag104prime-)
(not (Flag104-))
)

)
(:action Prim105Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag105prime-)
(not (Flag105-))
)

)
(:action Prim105Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag105prime-)
(not (Flag105-))
)

)
(:action Prim106Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag106prime-)
(not (Flag106-))
)

)
(:action Prim106Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag106prime-)
(not (Flag106-))
)

)
(:action Prim107Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag107prime-)
(not (Flag107-))
)

)
(:action Prim107Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag107prime-)
(not (Flag107-))
)

)
(:action Prim108Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag108prime-)
(not (Flag108-))
)

)
(:action Prim108Action-1
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag108prime-)
(not (Flag108-))
)

)
(:action Prim109Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag109prime-)
(not (Flag109-))
)

)
(:action Prim109Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag109prime-)
(not (Flag109-))
)

)
(:action Flag111Action
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag111-)
(Flag111prime-)
)

)
(:action Flag112Action
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag113Action
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag114Action
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Prim111Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag111prime-)
(not (Flag111-))
)

)
(:action Prim111Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag111prime-)
(not (Flag111-))
)

)
(:action Prim112Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag112prime-)
(not (Flag112-))
)

)
(:action Prim112Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag112prime-)
(not (Flag112-))
)

)
(:action Prim113Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag113prime-)
(not (Flag113-))
)

)
(:action Prim113Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag113prime-)
(not (Flag113-))
)

)
(:action Prim114Action-0
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag114prime-)
(not (Flag114-))
)

)
(:action Prim114Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag114prime-)
(not (Flag114-))
)

)
(:action Flag116Action
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag116-)
(Flag116prime-)
)

)
(:action Flag117Action
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag118Action
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Prim116Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag116prime-)
(not (Flag116-))
)

)
(:action Prim116Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag116prime-)
(not (Flag116-))
)

)
(:action Prim117Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag117prime-)
(not (Flag117-))
)

)
(:action Prim117Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag117prime-)
(not (Flag117-))
)

)
(:action Prim118Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag118prime-)
(not (Flag118-))
)

)
(:action Prim118Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag118prime-)
(not (Flag118-))
)

)
(:action Flag120Action
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag120-)
(Flag120prime-)
)

)
(:action Flag121Action
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag121-)
(Flag121prime-)
)

)
(:action Flag122Action
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag122-)
(Flag122prime-)
)

)
(:action Prim120Action-0
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag120prime-)
(not (Flag120-))
)

)
(:action Prim120Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag120prime-)
(not (Flag120-))
)

)
(:action Prim121Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag121prime-)
(not (Flag121-))
)

)
(:action Prim122Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag122prime-)
(not (Flag122-))
)

)
(:action Flag124Action-0
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag124-)
(Flag124prime-)
)

)
(:action Flag124Action-2
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag124-)
(Flag124prime-)
)

)
(:action Flag124Action-3
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag124-)
(Flag124prime-)
)

)
(:action Flag124Action-4
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag124-)
(Flag124prime-)
)

)
(:action Flag124Action-5
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag124-)
(Flag124prime-)
)

)
(:action Flag124Action-6
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag124-)
(Flag124prime-)
)

)
(:action Flag124Action-7
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag124-)
(Flag124prime-)
)

)
(:action Prim124Action
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag124prime-)
(not (Flag124-))
)

)
(:action Flag125Action
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag126Action
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag126-)
(Flag126prime-)
)

)
(:action Prim125Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag125prime-)
(not (Flag125-))
)

)
(:action Prim126Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag126prime-)
(not (Flag126-))
)

)
(:action Prim126Action-1
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag126prime-)
(not (Flag126-))
)

)
(:action MOVELEFT-ROBOT
:parameters ()
:precondition
(and
(Flag3-)
(Flag3prime-)
(Flag127prime-)
(Flag123prime-)
(Flag119prime-)
(Flag115prime-)
(Flag110prime-)
(Flag103prime-)
(Flag96prime-)
(Flag124prime-)
)
:effect
(and
(when
(and
(Flag127-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag123-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag119-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag115-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag110-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag103-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(Flag96-)
)
(and
(COLUMN0-ROBOT)
(not (NOT-COLUMN0-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN0-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN0-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(RIGHTOF7-ROBOT)
)
(and
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF6-ROBOT)
)
(and
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF5-ROBOT)
)
(and
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF4-ROBOT)
)
(and
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF3-ROBOT)
)
(and
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF2-ROBOT)
)
(and
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF2-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF1-ROBOT)
)
(and
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF0-ROBOT))
(not (RIGHTOF1-ROBOT))
)
)
(when
(and
(Flag124-)
)
(and
(LEFTOF7-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
)
)
(when
(and
(LEFTOF4-ROBOT)
)
(and
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF3-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
)
)
(when
(and
(LEFTOF3-ROBOT)
)
(and
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF5-ROBOT)
)
(and
(LEFTOF6-ROBOT)
(LEFTOF4-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
)
)
(when
(and
(LEFTOF6-ROBOT)
)
(and
(LEFTOF5-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
(not (NOT-LEFTOF1-ROBOT))
)
)
(when
(and
(LEFTOF7-ROBOT)
)
(and
(LEFTOF6-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
)
)
(when
(and
(LEFTOF2-ROBOT)
)
(and
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF1-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
(not (NOT-LEFTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag124prime-))
(not (Flag78prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag62prime-))
(not (Flag16prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag65prime-))
(not (Flag61prime-))
(not (Flag57prime-))
(not (Flag52prime-))
(not (Flag47prime-))
(not (Flag40prime-))
(not (Flag32prime-))
(not (Flag1prime-))
(not (Flag127prime-))
(not (Flag123prime-))
(not (Flag119prime-))
(not (Flag115prime-))
(not (Flag110prime-))
(not (Flag103prime-))
(not (Flag96prime-))
(not (Flag87prime-))
)
)
(:action MOVERIGHT-ROBOT
:parameters ()
:precondition
(and
(Flag3-)
(Flag3prime-)
(Flag123prime-)
(Flag119prime-)
(Flag115prime-)
(Flag110prime-)
(Flag103prime-)
(Flag96prime-)
(Flag87prime-)
(Flag78prime-)
(Flag77prime-)
(Flag76prime-)
(Flag75prime-)
(Flag74prime-)
(Flag73prime-)
(Flag72prime-)
(Flag71prime-)
(Flag70prime-)
(Flag69prime-)
(Flag68prime-)
(Flag67prime-)
(Flag66prime-)
)
:effect
(and
(when
(and
(Flag123-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag119-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag115-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag110-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag103-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag96-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag87-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN0-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN0-ROBOT))
)
)
(when
(and
(Flag78-)
)
(and
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
(not (LEFTOF7-ROBOT))
)
)
(when
(and
(Flag77-)
)
(and
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
(not (LEFTOF6-ROBOT))
)
)
(when
(and
(Flag76-)
)
(and
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
)
)
(when
(and
(Flag75-)
)
(and
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(not (NOT-LEFTOF5-ROBOT))
(not (LEFTOF4-ROBOT))
)
)
(when
(and
(Flag74-)
)
(and
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(not (NOT-LEFTOF4-ROBOT))
(not (LEFTOF3-ROBOT))
)
)
(when
(and
(Flag73-)
)
(and
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(not (NOT-LEFTOF3-ROBOT))
(not (LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(not (NOT-LEFTOF2-ROBOT))
(not (LEFTOF1-ROBOT))
)
)
(when
(and
(Flag72-)
)
(and
(RIGHTOF7-ROBOT)
(not (NOT-RIGHTOF7-ROBOT))
)
)
(when
(and
(Flag71-)
)
(and
(RIGHTOF6-ROBOT)
(not (NOT-RIGHTOF6-ROBOT))
)
)
(when
(and
(Flag70-)
)
(and
(RIGHTOF5-ROBOT)
(not (NOT-RIGHTOF5-ROBOT))
)
)
(when
(and
(Flag69-)
)
(and
(RIGHTOF4-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
)
)
(when
(and
(Flag68-)
)
(and
(RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
)
)
(when
(and
(Flag67-)
)
(and
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag66-)
)
(and
(RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag124prime-))
(not (Flag78prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag62prime-))
(not (Flag16prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag65prime-))
(not (Flag61prime-))
(not (Flag57prime-))
(not (Flag52prime-))
(not (Flag47prime-))
(not (Flag40prime-))
(not (Flag32prime-))
(not (Flag1prime-))
(not (Flag127prime-))
(not (Flag123prime-))
(not (Flag119prime-))
(not (Flag115prime-))
(not (Flag110prime-))
(not (Flag103prime-))
(not (Flag96prime-))
(not (Flag87prime-))
)
)
(:action Prim1Action-1
:parameters ()
:precondition
(and
(not (ROW1-ROBOT))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF1-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF2-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF2-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
(not (Flag124prime-))
(not (Flag78prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag62prime-))
(not (Flag16prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag65prime-))
(not (Flag61prime-))
(not (Flag57prime-))
(not (Flag52prime-))
(not (Flag47prime-))
(not (Flag40prime-))
(not (Flag32prime-))
(not (Flag1prime-))
(not (Flag127prime-))
(not (Flag123prime-))
(not (Flag119prime-))
(not (Flag115prime-))
(not (Flag110prime-))
(not (Flag103prime-))
(not (Flag96prime-))
(not (Flag87prime-))
)
)
(:action Flag3Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag2-)
(Flag2prime-)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Prim3Action-2
:parameters ()
:precondition
(and
(not (Flag2-))
(Flag2prime-)
)
:effect
(and
(Flag3prime-)
(not (Flag3-))
)

)
(:action Flag5Action-0
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-1
:parameters ()
:precondition
(and
(ABOVEOF1-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-2
:parameters ()
:precondition
(and
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-3
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-4
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-5
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-6
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Prim5Action
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF1-ROBOT))
(not (ABOVEOF2-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF3-ROBOT))
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag5prime-)
(not (Flag5-))
)

)
(:action Flag6Action-0
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-1
:parameters ()
:precondition
(and
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-2
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-3
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-4
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-5
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Prim6Action
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF2-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF3-ROBOT))
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag6prime-)
(not (Flag6-))
)

)
(:action Flag7Action-0
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-1
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-2
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-3
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-4
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Prim7Action
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF3-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF5-ROBOT))
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag7prime-)
(not (Flag7-))
)

)
(:action Flag8Action-0
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-1
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-2
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-3
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Prim8Action
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF5-ROBOT))
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag8prime-)
(not (Flag8-))
)

)
(:action Flag9Action-0
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-1
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-2
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Prim9Action
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF5-ROBOT))
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag9prime-)
(not (Flag9-))
)

)
(:action Flag10Action-0
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-1
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Prim10Action
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action Flag11Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Prim11Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag11prime-)
(not (Flag11-))
)

)
(:action Flag12Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-2
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Prim12Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag12prime-)
(not (Flag12-))
)

)
(:action Flag13Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-2
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-3
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Prim13Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag13prime-)
(not (Flag13-))
)

)
(:action Flag14Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-2
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-3
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-4
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Prim14Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag14prime-)
(not (Flag14-))
)

)
(:action Flag15Action-0
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-1
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-2
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-3
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-4
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-5
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Prim15Action
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag15prime-)
(not (Flag15-))
)

)
(:action Flag16Action-0
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-1
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-2
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-3
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-4
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-6
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Prim16Action
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag16prime-)
(not (Flag16-))
)

)
(:action Flag17Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag18Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag19Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag20Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag21Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF1-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag22Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag23Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag24Action-6
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Prim17Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag17prime-)
(not (Flag17-))
)

)
(:action Prim17Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag17prime-)
(not (Flag17-))
)

)
(:action Prim18Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag18prime-)
(not (Flag18-))
)

)
(:action Prim18Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag18prime-)
(not (Flag18-))
)

)
(:action Prim19Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag19prime-)
(not (Flag19-))
)

)
(:action Prim19Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag19prime-)
(not (Flag19-))
)

)
(:action Prim20Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag20prime-)
(not (Flag20-))
)

)
(:action Prim20Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag20prime-)
(not (Flag20-))
)

)
(:action Prim21Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Prim21Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF1-ROBOT))
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Prim22Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag22prime-)
(not (Flag22-))
)

)
(:action Prim22Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag22prime-)
(not (Flag22-))
)

)
(:action Prim23Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag23prime-)
(not (Flag23-))
)

)
(:action Prim23Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag23prime-)
(not (Flag23-))
)

)
(:action Flag25Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag26Action
:parameters ()
:precondition
(and
(ABOVEOF2-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag27Action
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag28Action
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag29Action
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag30Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag31Action
:parameters ()
:precondition
(and
(ABOVEOF1-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Prim25Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim26Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag26prime-)
(not (Flag26-))
)

)
(:action Prim26Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag26prime-)
(not (Flag26-))
)

)
(:action Prim27Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim27Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim28Action-0
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag28prime-)
(not (Flag28-))
)

)
(:action Prim28Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag28prime-)
(not (Flag28-))
)

)
(:action Prim29Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim29Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim30Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag30prime-)
(not (Flag30-))
)

)
(:action Prim30Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag30prime-)
(not (Flag30-))
)

)
(:action Prim31Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF1-ROBOT))
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim31Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Flag33Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag34Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Flag35Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag36Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag36-)
(Flag36prime-)
)

)
(:action Flag37Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag37-)
(Flag37prime-)
)

)
(:action Flag38Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag39Action
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag39-)
(Flag39prime-)
)

)
(:action Prim33Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag33prime-)
(not (Flag33-))
)

)
(:action Prim33Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag33prime-)
(not (Flag33-))
)

)
(:action Prim34Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag34prime-)
(not (Flag34-))
)

)
(:action Prim34Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag34prime-)
(not (Flag34-))
)

)
(:action Prim35Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-1
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim36Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag36prime-)
(not (Flag36-))
)

)
(:action Prim36Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag36prime-)
(not (Flag36-))
)

)
(:action Prim37Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim38Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag38prime-)
(not (Flag38-))
)

)
(:action Prim38Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag38prime-)
(not (Flag38-))
)

)
(:action Prim39Action-0
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim39Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Flag41Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag42Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Flag43Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Flag44Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag44-)
(Flag44prime-)
)

)
(:action Flag45Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag46Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag46-)
(Flag46prime-)
)

)
(:action Prim41Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim41Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim42Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag42prime-)
(not (Flag42-))
)

)
(:action Prim42Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag42prime-)
(not (Flag42-))
)

)
(:action Prim43Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim44Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim44Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim45Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag45prime-)
(not (Flag45-))
)

)
(:action Prim45Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag45prime-)
(not (Flag45-))
)

)
(:action Prim46Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag46prime-)
(not (Flag46-))
)

)
(:action Prim46Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag46prime-)
(not (Flag46-))
)

)
(:action Flag48Action
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag49Action
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag49-)
(Flag49prime-)
)

)
(:action Flag50Action
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag50-)
(Flag50prime-)
)

)
(:action Flag51Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag51-)
(Flag51prime-)
)

)
(:action Prim48Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag48prime-)
(not (Flag48-))
)

)
(:action Prim48Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag48prime-)
(not (Flag48-))
)

)
(:action Prim49Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag49prime-)
(not (Flag49-))
)

)
(:action Prim49Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag49prime-)
(not (Flag49-))
)

)
(:action Prim50Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag50prime-)
(not (Flag50-))
)

)
(:action Prim50Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag50prime-)
(not (Flag50-))
)

)
(:action Prim51Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag51prime-)
(not (Flag51-))
)

)
(:action Prim51Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag51prime-)
(not (Flag51-))
)

)
(:action Flag53Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag53-)
(Flag53prime-)
)

)
(:action Flag54Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag54-)
(Flag54prime-)
)

)
(:action Flag55Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag55-)
(Flag55prime-)
)

)
(:action Flag56Action
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Prim53Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag53prime-)
(not (Flag53-))
)

)
(:action Prim53Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag53prime-)
(not (Flag53-))
)

)
(:action Prim54Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag54prime-)
(not (Flag54-))
)

)
(:action Prim54Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag54prime-)
(not (Flag54-))
)

)
(:action Prim55Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag55prime-)
(not (Flag55-))
)

)
(:action Prim55Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag55prime-)
(not (Flag55-))
)

)
(:action Prim56Action-0
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag56prime-)
(not (Flag56-))
)

)
(:action Prim56Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag56prime-)
(not (Flag56-))
)

)
(:action Flag58Action
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag58-)
(Flag58prime-)
)

)
(:action Flag59Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag59-)
(Flag59prime-)
)

)
(:action Flag60Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Prim58Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag58prime-)
(not (Flag58-))
)

)
(:action Prim59Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag59prime-)
(not (Flag59-))
)

)
(:action Prim60Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag60prime-)
(not (Flag60-))
)

)
(:action Prim60Action-1
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag60prime-)
(not (Flag60-))
)

)
(:action Flag62Action-0
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag62-)
(Flag62prime-)
)

)
(:action Flag62Action-1
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag62-)
(Flag62prime-)
)

)
(:action Flag62Action-2
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag62-)
(Flag62prime-)
)

)
(:action Flag62Action-3
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag62-)
(Flag62prime-)
)

)
(:action Flag62Action-4
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag62-)
(Flag62prime-)
)

)
(:action Flag62Action-6
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag62-)
(Flag62prime-)
)

)
(:action Flag62Action-7
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag62-)
(Flag62prime-)
)

)
(:action Prim62Action
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag62prime-)
(not (Flag62-))
)

)
(:action Flag63Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag63-)
(Flag63prime-)
)

)
(:action Flag64Action
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag64-)
(Flag64prime-)
)

)
(:action Prim63Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag63prime-)
(not (Flag63-))
)

)
(:action Prim63Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag63prime-)
(not (Flag63-))
)

)
(:action Prim64Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag64prime-)
(not (Flag64-))
)

)
(:action MOVEDOWN-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
(Flag4prime-)
(Flag65prime-)
(Flag61prime-)
(Flag57prime-)
(Flag52prime-)
(Flag47prime-)
(Flag40prime-)
(Flag32prime-)
(Flag62prime-)
)
:effect
(and
(when
(and
(Flag65-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag61-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag57-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag52-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag47-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag40-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(Flag32-)
)
(and
(ROW0-ROBOT)
(not (NOT-ROW0-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW0-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW0-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ABOVEOF7-ROBOT)
)
(and
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF7-ROBOT))
)
)
(when
(and
(ABOVEOF6-ROBOT)
)
(and
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF6-ROBOT))
)
)
(when
(and
(ABOVEOF5-ROBOT)
)
(and
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF5-ROBOT))
)
)
(when
(and
(ABOVEOF4-ROBOT)
)
(and
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF4-ROBOT))
)
)
(when
(and
(ABOVEOF3-ROBOT)
)
(and
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF3-ROBOT))
)
)
(when
(and
(ABOVEOF2-ROBOT)
)
(and
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF1-ROBOT)
)
(and
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF1-ROBOT))
)
)
(when
(and
(Flag62-)
)
(and
(BELOWOF7-ROBOT)
(not (NOT-BELOWOF7-ROBOT))
)
)
(when
(and
(BELOWOF3-ROBOT)
)
(and
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF7-ROBOT)
)
(and
(BELOWOF6-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
)
)
(when
(and
(BELOWOF4-ROBOT)
)
(and
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF3-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
(not (NOT-BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF2-ROBOT)
)
(and
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF1-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
(not (NOT-BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF5-ROBOT)
)
(and
(BELOWOF6-ROBOT)
(BELOWOF4-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
)
)
(when
(and
(BELOWOF6-ROBOT)
)
(and
(BELOWOF5-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag124prime-))
(not (Flag78prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag62prime-))
(not (Flag16prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag65prime-))
(not (Flag61prime-))
(not (Flag57prime-))
(not (Flag52prime-))
(not (Flag47prime-))
(not (Flag40prime-))
(not (Flag32prime-))
(not (Flag1prime-))
(not (Flag127prime-))
(not (Flag123prime-))
(not (Flag119prime-))
(not (Flag115prime-))
(not (Flag110prime-))
(not (Flag103prime-))
(not (Flag96prime-))
(not (Flag87prime-))
)
)
(:action MOVEUP-ROBOT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag61prime-)
(Flag57prime-)
(Flag52prime-)
(Flag47prime-)
(Flag40prime-)
(Flag32prime-)
(Flag24prime-)
(Flag16prime-)
(Flag15prime-)
(Flag14prime-)
(Flag13prime-)
(Flag12prime-)
(Flag11prime-)
(Flag10prime-)
(Flag9prime-)
(Flag8prime-)
(Flag7prime-)
(Flag6prime-)
(Flag5prime-)
)
:effect
(and
(when
(and
(Flag61-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag57-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag52-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag47-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag40-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag32-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag24-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ROW0-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW0-ROBOT))
)
)
(when
(and
(Flag16-)
)
(and
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(not (NOT-BELOWOF8-ROBOT))
(not (BELOWOF7-ROBOT))
)
)
(when
(and
(Flag15-)
)
(and
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(not (NOT-BELOWOF7-ROBOT))
(not (BELOWOF6-ROBOT))
)
)
(when
(and
(Flag14-)
)
(and
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
)
)
(when
(and
(Flag13-)
)
(and
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
(not (BELOWOF4-ROBOT))
)
)
(when
(and
(Flag12-)
)
(and
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
(not (BELOWOF3-ROBOT))
)
)
(when
(and
(Flag11-)
)
(and
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(not (NOT-BELOWOF3-ROBOT))
(not (BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(not (NOT-BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
)
)
(when
(and
(Flag10-)
)
(and
(ABOVEOF7-ROBOT)
(not (NOT-ABOVEOF7-ROBOT))
)
)
(when
(and
(Flag9-)
)
(and
(ABOVEOF6-ROBOT)
(not (NOT-ABOVEOF6-ROBOT))
)
)
(when
(and
(Flag8-)
)
(and
(ABOVEOF5-ROBOT)
(not (NOT-ABOVEOF5-ROBOT))
)
)
(when
(and
(Flag7-)
)
(and
(ABOVEOF4-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
)
)
(when
(and
(Flag6-)
)
(and
(ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
)
)
(when
(and
(Flag5-)
)
(and
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
)
)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(not (NOT-ABOVEOF1-ROBOT))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag124prime-))
(not (Flag78prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag62prime-))
(not (Flag16prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag65prime-))
(not (Flag61prime-))
(not (Flag57prime-))
(not (Flag52prime-))
(not (Flag47prime-))
(not (Flag40prime-))
(not (Flag32prime-))
(not (Flag1prime-))
(not (Flag127prime-))
(not (Flag123prime-))
(not (Flag119prime-))
(not (Flag115prime-))
(not (Flag110prime-))
(not (Flag103prime-))
(not (Flag96prime-))
(not (Flag87prime-))
)
)
(:action Prim1Action-2
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Prim1Action-3
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Flag2Action-9
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-11
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Prim3Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag3prime-)
(not (Flag3-))
)

)
(:action Prim3Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag3prime-)
(not (Flag3-))
)

)
(:action Flag4Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Prim4Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Prim4Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Flag16Action-5
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Prim58Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag58prime-)
(not (Flag58-))
)

)
(:action Prim59Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag59prime-)
(not (Flag59-))
)

)
(:action Flag62Action-5
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag62-)
(Flag62prime-)
)

)
(:action Prim64Action-0
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag64prime-)
(not (Flag64-))
)

)
(:action Flag66Action-2
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Flag67Action-2
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag68Action-2
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag68-)
(Flag68prime-)
)

)
(:action Flag78Action-1
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag78-)
(Flag78prime-)
)

)
(:action Prim85Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag85prime-)
(not (Flag85-))
)

)
(:action Prim92Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag92prime-)
(not (Flag92-))
)

)
(:action Prim102Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag102prime-)
(not (Flag102-))
)

)
(:action Prim121Action-0
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag121prime-)
(not (Flag121-))
)

)
(:action Prim122Action-0
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag122prime-)
(not (Flag122-))
)

)
(:action Flag124Action-1
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag124-)
(Flag124prime-)
)

)
(:action Prim125Action-1
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag125prime-)
(not (Flag125-))
)

)
)
